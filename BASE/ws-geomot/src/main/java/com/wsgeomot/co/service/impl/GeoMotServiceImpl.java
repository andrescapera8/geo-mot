/**
 * 
 */
package com.wsgeomot.co.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wsgeomot.co.dao.GeoMotDao;
import com.wsgeomot.co.model.dto.ContactoDTO;
import com.wsgeomot.co.model.dto.ControlEstadoMotoDTO;
import com.wsgeomot.co.model.dto.ControlEstadoMotoDTOList;
import com.wsgeomot.co.model.dto.GeneralData;
import com.wsgeomot.co.model.dto.KilometrajenotiListDTO;
import com.wsgeomot.co.model.dto.KilometrajenotificacionDTO;
import com.wsgeomot.co.model.dto.MotoDTO;
import com.wsgeomot.co.model.dto.PersonaContactoRequest;
import com.wsgeomot.co.model.dto.PersonaDTO;
import com.wsgeomot.co.model.dto.RequesInfoGeneral;
import com.wsgeomot.co.model.dto.StatusResponse;
import com.wsgeomot.co.model.entity.ContactoEntity;
import com.wsgeomot.co.model.entity.ControlEstadoMotoEntity;
import com.wsgeomot.co.model.entity.KilometrajenotificacionEntity;
import com.wsgeomot.co.model.entity.MotoEntity;
import com.wsgeomot.co.model.entity.PersonaEntity;
import com.wsgeomot.co.model.response.ControlEstadoMotoDTOResponse;
import com.wsgeomot.co.model.response.ControlEstadoMotoDTOResponseGN;
import com.wsgeomot.co.model.response.ResponseContEstMotoDTOList;
import com.wsgeomot.co.model.response.ResponseContacto;
import com.wsgeomot.co.model.response.ResponseContactoEntity;
import com.wsgeomot.co.model.response.ResponseContactoGL;
import com.wsgeomot.co.model.response.ResponseEventoPlantillaDTO;
import com.wsgeomot.co.model.response.ResponseKilometrajeNotiList;
import com.wsgeomot.co.model.response.ResponseKilometrajenotificacion;
import com.wsgeomot.co.model.response.ResponseMaestraLista;
import com.wsgeomot.co.model.response.ResponseMotoEntity;
import com.wsgeomot.co.model.response.ResponseNotificaiones;
import com.wsgeomot.co.model.response.ResponsePersonaEntity;
import com.wsgeomot.co.service.GeoMotService;
import com.wsgeomot.util.GeneralMetodos;
import com.wsgeomot.util.ResponseCodes;
import com.wsgeomot.util.ValidateGeneral;

/**
 * @author Andres Capera
 *
 */
@Service
@Transactional
public class GeoMotServiceImpl implements GeoMotService {
	
	private GeneralMetodos valid = new GeneralMetodos();

	@Autowired
	private GeoMotDao service;

	ValidateGeneral validateGeneral = new ValidateGeneral();

	private static final Logger logger = Logger.getLogger(GeoMotServiceImpl.class);

	public static final int VL = 1;
	public static final Integer KL = 0;
	
	public static final String paramFinal = "FILTER_VEHICLE";
	

	/**
	 * METODO DE CONSULTA DE INFORMACION GENERAL PERSONA MOTO
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return RequesInfoGeneral
	 */
	public RequesInfoGeneral getInfoMotoPersona(Integer tipoDocumento, String numDocumento, String placa, String param) {
		RequesInfoGeneral requesInfoGeneral = new RequesInfoGeneral();
		try {			
			requesInfoGeneral = service.getInfoMotoPersona(tipoDocumento, numDocumento, placa, param.equals(paramFinal) ? null : param);
			if(requesInfoGeneral.getStatusResponse().equals(ResponseCodes.SUCCESS) || requesInfoGeneral.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {			
				if (param != null && param.equals(paramFinal)) {
					GeneralData generalData = new GeneralData();
					generalData.setType(tipoDocumento);
					generalData.setDoc(numDocumento);
					generalData.setPers(requesInfoGeneral.getPersonaDTO() != null ? requesInfoGeneral.getPersonaDTO().getIdPersona() : null);
					generalData.setPlaca(requesInfoGeneral.getMotoDTO() != null ? requesInfoGeneral.getMotoDTO().getPlaca() : null);
					generalData.setVehicleId(requesInfoGeneral.getMotoDTO() != null ? requesInfoGeneral.getMotoDTO().getIdMoto() : null);
					requesInfoGeneral.setGeneralData(generalData);
					requesInfoGeneral.setContactoDTOList(null);
					requesInfoGeneral.setKilometrajenotificacionDTOList(null);
					requesInfoGeneral.setPersonaDTO(null);
					requesInfoGeneral.setMotoDTO(null);
					StatusResponse statusResponse = validatePages(generalData, requesInfoGeneral.getKilometrajenotificacionDTOList() != null ? requesInfoGeneral.getKilometrajenotificacionDTOList().isEmpty() : Boolean.FALSE);
					requesInfoGeneral.setScreen(statusResponse.getDescription());
					requesInfoGeneral.setStatusResponse(ResponseCodes.SUCCESS);
				}
			}
		} catch (Exception e) {
			requesInfoGeneral.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "getInfoMotoPersona " + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return requesInfoGeneral;
	}
	
	private StatusResponse validatePages(GeneralData generalData, Boolean getKilometrajenotificacionDTOList) {
		StatusResponse statusResponse = ResponseCodes.BLUETOOTH;
		if (generalData.getPers() != null) {
			if (generalData.getVehicleId() != null) {
				if (!getKilometrajenotificacionDTOList) {
					statusResponse = ResponseCodes.EVENTS_PAGE;
				}
			} else {
				statusResponse = ResponseCodes.VEHICLE_PAGE;
			}

		} else {
			statusResponse = ResponseCodes.PERSONAL_DATA_PAGE;
		}
		return statusResponse;
	}

	/**
	 * METODO DE CONSULTA DE EVENTOS Y PLANTILLAS
	 * 
	 * @param idEvento
	 * @returnResponseEventoPlantillaDTO
	 */
	public ResponseEventoPlantillaDTO getEventoPlantilla(Integer idEvento) {
		return service.getEventoPlantilla(idEvento);
	}

	/**
	 * METODO DE CONSULTA DE LISTAS MAESTRAS
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return ResponseMaestraLista
	 */
	public ResponseMaestraLista getInfoListasMaestras(String tipoDato) {
		return service.getInfoListasMaestras(tipoDato);
	}
	
	/**
	 * METODO DE CONSULTA DE NOTIFICAIONES
	 * 
	 * @param mtIdMoto
	 * @return ResponseNotificaiones
	 */
	public ResponseNotificaiones getNotificaiones(Integer mtIdMoto) {
		return service.getNotificaiones(mtIdMoto);
	}
	
	/**
	 * METODO DE CONSULTA DE RECORRIDOS
	 * 
	 * @param idMoto, idEvento
	 * @return ResponseContEstMotoDTOList
	 */
	@Override
	public ResponseContEstMotoDTOList getControlEstadoMoto(Integer idMoto, Integer idEvento) {
		return service.getControlEstadoMoto(idMoto, idEvento);
	}

	/**
	 * METODO DE REGISTRO DE PERSONA Y CONTACTO
	 * 
	 * @param persoContacRequest
	 * @return ResponseContactoGL
	 */
	public ResponseContactoGL insertUpdatePersonaContactoDTO(PersonaContactoRequest persoContacRequest) {
		StatusResponse statusResponse = ResponseCodes.SUCCESS;
		ResponseContactoGL responseContactoGL = new ResponseContactoGL();
		ResponsePersonaEntity responsePersonaEntity = new ResponsePersonaEntity();
		try {
			statusResponse = validateGeneral.validate(persoContacRequest.getPersonaDTO());
			
			if (statusResponse.getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {
				RequesInfoGeneral requesInfoGeneral = service.getInfoMotoPersona(persoContacRequest.getPersonaDTO().getTipoDocumento(), persoContacRequest.getPersonaDTO().getNumDocumento(), null, "PERSONA_CONTACTO");
		
				if (requesInfoGeneral.getStatusResponse().equals(ResponseCodes.SUCCESS) || requesInfoGeneral.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
					
					persoContacRequest.getPersonaDTO().setIdPersona(requesInfoGeneral.getStatusResponse().equals(ResponseCodes.SUCCESS) ?
					requesInfoGeneral.getPersonaDTO().getIdPersona() : requesInfoGeneral.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND) ? persoContacRequest.getPersonaDTO().getIdPersona():  null);
					responsePersonaEntity = insertUpdatetPersona(maperPersonaDTOToEntity(persoContacRequest.getPersonaDTO()));
					responseContactoGL.setStatusResponse(responsePersonaEntity.getStatusResponse());
					
				} else {
					responseContactoGL.setStatusResponse(requesInfoGeneral.getStatusResponse());
				}

				if (responseContactoGL.getStatusResponse().getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {
					responseContactoGL = insertUpdateContactoListDTO(persoContacRequest.getContactoDTOList(), responsePersonaEntity.getPersonaEntity().getIdPersona());
					responseContactoGL.setPersona(responsePersonaEntity.getPersonaEntity());
				}
			} else {
				responseContactoGL.setStatusResponse(statusResponse);
			}
			

		} catch (Exception e) {
			responseContactoGL.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateGeneral" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseContactoGL;

	}

	/**
	 * METODO DE RESGISTRO O ACTUALIZACION DE PERSONA
	 * 
	 * @param PersonaDTO
	 * @return insertUpdatePersonaDTO
	 */
	public ResponsePersonaEntity insertUpdatePersonaDTO(PersonaDTO persona) {
		ResponsePersonaEntity responsePersonaEntity = new ResponsePersonaEntity();
		try {
			StatusResponse statusResponse = validateGeneral.validate(persona);
			if (statusResponse.getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {
				responsePersonaEntity = insertUpdatetPersona(maperPersonaDTOToEntity(persona));
			} else {
				responsePersonaEntity.setStatusResponse(statusResponse);
			}
		} catch (Exception e) {
			responsePersonaEntity.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateGeneral" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responsePersonaEntity;

	}

	/**
	 * METODO DE REGISTRO O ACTUALIZACION CONTACTO DE PERSONA
	 * 
	 * @param ContactoDTO
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdateContactoDTO(ContactoDTO contacto) {
		StatusResponse statusResponse = ResponseCodes.SUCCESS;
		ResponseContactoEntity responseContactoEntity = new ResponseContactoEntity();
		try {
			statusResponse = validateGeneral.validate(contacto);
			if (statusResponse.getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {

				responseContactoEntity = service.getContactoEntity(contacto.getIdPersona(), contacto.getTipocontacto(), contacto.getValor());
				
				if (contacto.getIdContacto() == null) {
					if (responseContactoEntity.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
						statusResponse = insertUpdatetContacto(maperContactoDTOToEntity(contacto));
					} else if (responseContactoEntity.getStatusResponse().equals(ResponseCodes.SUCCESS)) {
						statusResponse = ResponseCodes.SUCCESS;
					} else {
						statusResponse = responseContactoEntity.getStatusResponse();
					}
				} else {
					statusResponse = insertUpdateContactoDTOlOGIC(contacto, responseContactoEntity);
				}
			}
		} catch (Exception e) {
			statusResponse = ResponseCodes.TECHNICAL_ERROR;
			String message = "GeoMotServiceImpl insertUpdateContactoDTO" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return statusResponse;

	}
	
	/**
	 * METODO DE ACTUALIZACION CONTACTO DE PERSONA LOGICA
	 * 
	 * @param ContactoDTO
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdateContactoDTOlOGIC(ContactoDTO contacto, ResponseContactoEntity responseContactoEntity) {
		StatusResponse statusResponse = ResponseCodes.UNPROCESSABLE_ENTITY;
		Boolean state = Boolean.TRUE;
		try {
			if (responseContactoEntity.getStatusResponse().equals(ResponseCodes.SUCCESS) && !responseContactoEntity.getContactoEntity().getIdContacto().equals(contacto.getIdContacto())) {
				state = Boolean.FALSE;
			}
			if (responseContactoEntity.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
				ResponseContactoEntity responContaEntity = service.getContactoIdEntity(contacto.getIdContacto());
				if(responContaEntity.getStatusResponse().equals(ResponseCodes.SUCCESS)) {					
					state = Boolean.TRUE;
				} else if (responContaEntity.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
					state = Boolean.TRUE;
				} else {
					statusResponse = responContaEntity.getStatusResponse();
					state = Boolean.FALSE;
				}
			}
			
			if (responseContactoEntity.getStatusResponse().equals(ResponseCodes.DATABASE_EXCEPTION)) {
				statusResponse = responseContactoEntity.getStatusResponse();
				state = Boolean.FALSE;
			}

			if (Boolean.TRUE.equals(state)) {
				statusResponse = insertUpdatetContacto(maperContactoDTOToEntity(contacto));
			}
		} catch (Exception e) {
			statusResponse = ResponseCodes.TECHNICAL_ERROR;
			String message = "GeoMotServiceImpl insertUpdateContactoDTO" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return statusResponse;

	}

	/**
	 * 
	 * @param contactoDTOList
	 * @return ResponseContactoGL
	 */
	public ResponseContactoGL insertUpdateContactoListDTO(List<ContactoDTO> contactoDTOList, String  idperson) {
		ResponseContactoGL responseContactoGL = new ResponseContactoGL();
		StatusResponse statusResponse = ResponseCodes.SUCCESS;
		List<ResponseContacto> responseContacto = new ArrayList<>();
		try {
			responseContactoGL.setStatusResponse(ResponseCodes.SUCCESS);
			if (!contactoDTOList.isEmpty() && (contactoDTOList.get(0) != null)) {
				for (ContactoDTO contactoDTO : contactoDTOList) {
					ResponseContacto listaRest = new ResponseContacto();
					contactoDTO.setIdPersona(idperson);
					statusResponse = insertUpdateContactoDTO(contactoDTO);
					listaRest.setStatusResponse(statusResponse);
					listaRest.setContacto(contactoDTO);
					responseContacto.add(listaRest);
				}
				responseContactoGL.setStatusResponse(ResponseCodes.SUCCESS);
				responseContactoGL.setResponseContacto(responseContacto);
			}
		} catch (Exception e) {
			responseContactoGL.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateContactoListDTO" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseContactoGL;

	}

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE MOTO
	 * 
	 * @param MotoDTO
	 * @return StatusResponse
	 */
	public ResponseMotoEntity insertUpdateMotoDTO(MotoDTO motoDTO) {
		ResponseMotoEntity responseMotoEntity = new ResponseMotoEntity();
		try {
			StatusResponse statusResponse = validateGeneral.validate(motoDTO);
			if (statusResponse.getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {

				ResponseMotoEntity responseMotoplaca = service.getMotoPlacaEntity(motoDTO.getPlaca());

				if (responseMotoplaca.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {

					responseMotoEntity = insertUpdateMotoDTOValidate(motoDTO);
				} else if (responseMotoplaca.getStatusResponse().equals(ResponseCodes.SUCCESS)) {
					if (responseMotoplaca.getMotoEntity().getIdPersona().equals(motoDTO.getIdPersona())) {
						responseMotoEntity = insertUpdateMotoDTOValidate(motoDTO);
					} else {
						responseMotoEntity.setStatusResponse(ResponseCodes.REGISTERED_MOTORCYCLE);
					}
				} else {
					responseMotoEntity.setStatusResponse(responseMotoplaca.getStatusResponse());
				}
			} else {
				responseMotoEntity.setStatusResponse(statusResponse);
			}
		} catch (Exception e) {
			responseMotoEntity.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateMotoDTO" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseMotoEntity;
	}
	
	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE MOTO
	 * 
	 * @param MotoDTO
	 * @return StatusResponse
	 */
	public ResponseMotoEntity insertUpdateMotoDTOValidate(MotoDTO motoDTO) {
		ResponseMotoEntity responseMotoEntity = new ResponseMotoEntity();
		try {

			ResponseMotoEntity resMotoEntity = service.getMotoEntity(motoDTO.getIdPersona(), motoDTO.getPlaca());
			if (motoDTO.getIdMoto() == null) {
				if (resMotoEntity.getStatusResponse().getStatus().equals(ResponseCodes.DATA_NOT_FOUND.getStatus())) {

					responseMotoEntity = insertUpdatetMoto(maperMotoDTOToEntity(motoDTO));
				} else if (resMotoEntity.getStatusResponse().getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {
	
						responseMotoEntity.setStatusResponse(ResponseCodes.SUCCESS);
						responseMotoEntity.setMotoEntity(resMotoEntity.getMotoEntity());
				} else {
					responseMotoEntity.setStatusResponse(resMotoEntity.getStatusResponse());
				}
			} else {
				responseMotoEntity = insertUpdateMotoDTOLogic(motoDTO, resMotoEntity);
			}
		} catch (Exception e) {
			responseMotoEntity.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateMotoDTOValidate" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseMotoEntity;
	}
	
	
	
	
	
	/**
	 * METODO DE ACTUALIZACION DE MOTO LOGICA
	 * 
	 * @param MotoDTO
	 * @return StatusResponse
	 */
	public ResponseMotoEntity insertUpdateMotoDTOLogic(MotoDTO motoDTO, ResponseMotoEntity resMotoEntity) {
		ResponseMotoEntity responseMotoEntity = new ResponseMotoEntity();
		responseMotoEntity.setStatusResponse(ResponseCodes.UNPROCESSABLE_ENTITY);
		Boolean state = Boolean.TRUE;
		try {
			if (resMotoEntity.getStatusResponse().equals(ResponseCodes.SUCCESS) && !resMotoEntity.getMotoEntity().getIdMoto().equals(motoDTO.getIdMoto())) {
				state = Boolean.FALSE;
			}
			if (resMotoEntity.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
				ResponseMotoEntity respMotoEntity = service.getMotoIdEntity(motoDTO.getIdMoto());
				if(respMotoEntity.getStatusResponse().equals(ResponseCodes.SUCCESS)) {					
					state = Boolean.TRUE;
				} else if (respMotoEntity.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
					state = Boolean.FALSE;
				} else {
					responseMotoEntity.setStatusResponse(respMotoEntity.getStatusResponse());
					state = Boolean.FALSE;
				}
			}
			
			if (responseMotoEntity.getStatusResponse().equals(ResponseCodes.DATABASE_EXCEPTION)) {
				responseMotoEntity.setStatusResponse(resMotoEntity.getStatusResponse());
				state = Boolean.FALSE;
			}
			
			if(Boolean.TRUE.equals(state)) {
				responseMotoEntity = insertUpdatetMoto(maperMotoDTOToEntity(motoDTO));
			}
		} catch (Exception e) {
			responseMotoEntity.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateMotoDTOLogic" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseMotoEntity;
	}

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE ESTADO KILOMETRAJE MOTO
	 * 
	 * @param ControlEstadoMotoDTO
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdateContEstadoMotoDTO(ControlEstadoMotoDTO controlEstadoMoto) {
		StatusResponse statusResponse = ResponseCodes.SUCCESS;
		try {
			statusResponse = validateGeneral.validate(controlEstadoMoto);
			if (statusResponse.getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {
				statusResponse = insertUpdatetControlEstadoMoto(maperControlEstadoMotoDTOToEntity(controlEstadoMoto));
			}
		} catch (Exception e) {
			statusResponse = ResponseCodes.TECHNICAL_ERROR;
			String message = "GeoMotServiceImpl insertUpdateContEstadoMotoDTO" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return statusResponse;

	}
	
	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE LISTA ESTADO KILOMETRAJE MOTO
	 * 
	 * @param ControlEstadoMotoDTO
	 * @return StatusResponse
	 */
	public ControlEstadoMotoDTOResponseGN insertUpdateContEstadoMotoDTOList(ControlEstadoMotoDTOList controlEstadoMotoList) {
		ControlEstadoMotoDTOResponseGN controlEstadoMotoDTOResponseGN = new ControlEstadoMotoDTOResponseGN();
		StatusResponse statusResponse = ResponseCodes.SUCCESS;
		List<ControlEstadoMotoDTOResponse> controlEstadoMotoDTOResponse = new ArrayList<>();
		Integer idMoto = 0;
		try {
			for (ControlEstadoMotoDTO dto : controlEstadoMotoList.getControlEstadoMotoDTO()) {
				ControlEstadoMotoDTOResponse controlEstadoMotoDTORespo = new ControlEstadoMotoDTOResponse();
				statusResponse = validateGeneral.validate(dto);
				if (statusResponse.getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {
					statusResponse = insertUpdatetControlEstadoMoto(maperControlEstadoMotoDTOToEntity(dto));
					controlEstadoMotoDTORespo.setControlEstadoMotoDTO(dto);
					controlEstadoMotoDTORespo.setStatusResponse(statusResponse);
				}
				controlEstadoMotoDTORespo.setControlEstadoMotoDTO(dto);
				controlEstadoMotoDTORespo.setStatusResponse(statusResponse);
				controlEstadoMotoDTOResponse.add(controlEstadoMotoDTORespo);

			}
			controlEstadoMotoDTOResponseGN.setControlEstadoMotoDTOResponse(controlEstadoMotoDTOResponse);
			controlEstadoMotoDTOResponseGN.setResponseNotificaiones(getNotificaiones(idMoto));
			controlEstadoMotoDTOResponseGN.setStatusResponse(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			controlEstadoMotoDTOResponseGN.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateContEstadoMotoDTOList" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return controlEstadoMotoDTOResponseGN;
	}

	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO
	 * 
	 * @param KilometrajenotificacionDTO
	 * @return StatusResponse
	 */
	public ResponseKilometrajenotificacion insertUpdateKilometrajenotificacionDTO(KilometrajenotificacionDTO kilometrajenotificacion) {
		ResponseKilometrajenotificacion responseKilometrajenotificacion = new ResponseKilometrajenotificacion();
		try {
			StatusResponse statusResponse = validateGeneral.validate(kilometrajenotificacion);
			if (statusResponse.getStatus().equals(ResponseCodes.SUCCESS.getStatus())) {
				
				ResponseKilometrajenotificacion respKiljenotificacion = service.getKilometrajenotificacion(kilometrajenotificacion.getMtidmoto(), kilometrajenotificacion.getEvtidevento(), kilometrajenotificacion.getKilometrajevalidador(), kilometrajenotificacion.getKntkilometraje());
				if (kilometrajenotificacion.getKnidkilometrajenoti() == null) {
					if (respKiljenotificacion.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
						
						responseKilometrajenotificacion = insertUpdatetKilometrajenotificacion(maperKilometrajenotificacionDTOToEntity(kilometrajenotificacion));
					} else if (respKiljenotificacion.getStatusResponse().equals(ResponseCodes.SUCCESS)) {
						
						responseKilometrajenotificacion.setStatusResponse(ResponseCodes.EXISTINGEVENT);
					} else {
						responseKilometrajenotificacion = respKiljenotificacion;

					}
				} else {
					responseKilometrajenotificacion = insertUpdateKilometrajenotificacionDTOLogic(kilometrajenotificacion, respKiljenotificacion);
				}
			} else {
				responseKilometrajenotificacion.setStatusResponse(statusResponse);
			}
		} catch (Exception e) {
			responseKilometrajenotificacion.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateKilometrajenotificacionDTO" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseKilometrajenotificacion;

	}
	
	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO LOGICA
	 * 
	 * @param KilometrajenotificacionDTO
	 * @return StatusResponse
	 */
	public ResponseKilometrajenotificacion insertUpdateKilometrajenotificacionDTOLogic(KilometrajenotificacionDTO kilnotifi, ResponseKilometrajenotificacion respKiljenotificacion) {
		ResponseKilometrajenotificacion responseKilometrajenotificacion = new ResponseKilometrajenotificacion();
		responseKilometrajenotificacion.setStatusResponse(ResponseCodes.UNPROCESSABLE_ENTITY);
		Boolean state = Boolean.TRUE;
		try {
			if (respKiljenotificacion.getStatusResponse().equals(ResponseCodes.SUCCESS) && 
					!kilnotifi.getKnidkilometrajenoti().equals(respKiljenotificacion.getKilometrajenotificacionEntity().getKnidkilometrajenoti())) {
				responseKilometrajenotificacion.setKilometrajenotificacionEntity(maperKilometrajenotificacionDTOToEntity(kilnotifi));
				state = Boolean.FALSE;
			}
			
			if (respKiljenotificacion.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
				ResponseKilometrajenotificacion responMotEntity = service.getKilometraIdjenotificacion(kilnotifi.getKnidkilometrajenoti());
				if(responMotEntity.getStatusResponse().equals(ResponseCodes.SUCCESS)) {	
					if(responMotEntity.getKilometrajenotificacionEntity().getMtidmoto().equals(kilnotifi.getMtidmoto())) {						
						state = Boolean.TRUE;
					}else {
						state = Boolean.FALSE;
					}
				} else if (responMotEntity.getStatusResponse().equals(ResponseCodes.DATA_NOT_FOUND)) {
					state = Boolean.FALSE;
				} else {
					responseKilometrajenotificacion.setStatusResponse(responMotEntity.getStatusResponse());
					state = Boolean.FALSE;
				}
			}
			
			if (respKiljenotificacion.getStatusResponse().equals(ResponseCodes.DATABASE_EXCEPTION)) {
				responseKilometrajenotificacion.setStatusResponse(respKiljenotificacion.getStatusResponse());
				state = Boolean.FALSE;
			}
			
			if (Boolean.TRUE.equals(state)) {
				responseKilometrajenotificacion = insertUpdatetKilometrajenotificacion(maperKilometrajenotificacionDTOToEntity(kilnotifi));
			}
			
		} catch (Exception e) {
			responseKilometrajenotificacion.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateKilometrajenotificacionDTOLogic" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseKilometrajenotificacion;
		
	}
	
	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO LISTA
	 * 
	 * @param KilometrajenotiListDTO
	 * @return ResponseKilometrajeNotiList
	 */
	public ResponseKilometrajeNotiList insertUpdateKilometrajenotificacionListDTO(KilometrajenotiListDTO kilometrajenotificacion) {
		ResponseKilometrajeNotiList responseKilometrajeNotiList = new ResponseKilometrajeNotiList();
		List<ResponseKilometrajenotificacion> responseKiljeNotiList = new ArrayList<>();
		try {
			for (KilometrajenotificacionDTO KilometrajeNoti : kilometrajenotificacion.getKilometrajenotificacionListDTO()) {
				ResponseKilometrajenotificacion responseKilometrajenotificacion = insertUpdateKilometrajenotificacionDTO(KilometrajeNoti);
				responseKiljeNotiList.add(responseKilometrajenotificacion);
			}
			responseKilometrajeNotiList.setResponseKilometrajeNotiList(responseKiljeNotiList);
			responseKilometrajeNotiList.setStatusResponse(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			responseKilometrajeNotiList.setStatusResponse(ResponseCodes.TECHNICAL_ERROR);
			String message = "GeoMotServiceImpl insertUpdateKilometrajenotificacionDTO" + e.getMessage() + e.getCause();
			logger.error(message);
		}
		return responseKilometrajeNotiList;

	}

	/**
	 * 
	 * @param PersonaEntity
	 * @return StatusResponse
	 */
	public ResponsePersonaEntity insertUpdatetPersona(PersonaEntity entity) {
		return service.insertUpdatetPersona(entity);
	}

	/**
	 * 
	 * @param PersonaDTO
	 * @return PersonaEntity
	 */
	public PersonaEntity maperPersonaDTOToEntity(PersonaDTO personaDTO) {
		PersonaEntity personaEntity = new PersonaEntity();

		if (personaDTO.getIdPersona() != null) {
			personaEntity.setIdPersona(personaDTO.getIdPersona());
		}

		personaEntity.setTipoDocumento(personaDTO.getTipoDocumento());
		personaEntity.setNumDocumento(personaDTO.getNumDocumento());
		personaEntity.setNombres(personaDTO.getNombres());
		personaEntity.setApellidos(personaDTO.getApellidos());
		personaEntity.setSexo(personaDTO.getSexo());
		personaEntity
				.setFirmaDigital(personaDTO.getFirmaDigital() != null ? personaDTO.getFirmaDigital().getBytes() : null);
		personaEntity.setFoto(personaDTO.getFoto() != null ? personaDTO.getFoto().getBytes() : null);
		personaEntity.setFechaNacimiento(personaDTO.getFechaNacimiento());
		personaEntity.setEstatura(personaDTO.getEstatura());
		personaEntity.setPeso(personaDTO.getPeso());
		personaEntity.setTipoRh(personaDTO.getTipoRh());
		personaEntity.setEstadoPersona(personaDTO.getEstadoPersona() != null ? personaDTO.getEstadoPersona() : VL);

		return personaEntity;
	}

	/**
	 * 
	 * @param ContactoEntity
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdatetContacto(ContactoEntity entity) {
		return service.insertUpdatetContacto(entity);
	}

	/**
	 * 
	 * @param ContactoDTO
	 * @return ContactoEntity
	 */
	public ContactoEntity maperContactoDTOToEntity(ContactoDTO contactoDTO) {
		ContactoEntity contactoEntity = new ContactoEntity();

		if (contactoDTO.getIdContacto() != null) {
			contactoEntity.setIdContacto(contactoDTO.getIdContacto());
		}
		contactoEntity.setIdPersona(contactoDTO.getIdPersona());
		contactoEntity.setTipocontacto(contactoDTO.getTipocontacto());
		contactoEntity.setValor(contactoDTO.getValor());
		contactoEntity
				.setEstadoContacto(contactoDTO.getEstadoContacto() != null ? contactoDTO.getEstadoContacto() : VL);

		return contactoEntity;

	}

	/**
	 * 
	 * @param MotoEntity
	 * @return StatusResponse
	 */
	public ResponseMotoEntity insertUpdatetMoto(MotoEntity entity) {
		return service.insertUpdatetMoto(entity);
	}

	/**
	 * 
	 * @param MotoDTO
	 * @return MotoEntity
	 */
	public MotoEntity maperMotoDTOToEntity(MotoDTO motoDTO) {
		MotoEntity motoEntity = new MotoEntity();

		if (motoDTO.getIdMoto() != null) {
			motoEntity.setIdMoto(motoDTO.getIdMoto());
		}
		motoEntity.setIdPersona(motoDTO.getIdPersona());
		motoEntity.setReferencia(motoDTO.getReferencia());
		motoEntity.setClase(motoDTO.getClase());
		motoEntity.setMarca(motoDTO.getMarca());
		motoEntity.setCleidcilindraje(motoDTO.getCleidcilindraje());
		motoEntity.setModelo(motoDTO.getModelo());
		motoEntity.setPlaca(motoDTO.getPlaca());
		motoEntity.setCombustilbe(motoDTO.getCombustilbe());
		motoEntity.setNumMotor(motoDTO.getNumMotor());
		motoEntity.setNumChasis(motoDTO.getNumChasis());
		motoEntity.setVin(motoDTO.getVin());
		motoEntity.setLinea(motoDTO.getLinea());
		motoEntity.setColor(motoDTO.getColor());
		motoEntity.setPeso(motoDTO.getPeso());
		motoEntity.setEstadoMoto(motoDTO.getEstadoMoto() != null ? motoDTO.getEstadoMoto() : VL);

		return motoEntity;

	}

	/**
	 * 
	 * @param ControlEstadoMotoEntity
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdatetControlEstadoMoto(ControlEstadoMotoEntity entity) {
		return service.insertUpdatetControlEstadoMoto(entity);
	}

	/**
	 * 
	 * @param ControlEstadoMotoDTO
	 * @return ControlEstadoMotoEntity
	 */
	public ControlEstadoMotoEntity maperControlEstadoMotoDTOToEntity(ControlEstadoMotoDTO controlDTO) {
		ControlEstadoMotoEntity entity = new ControlEstadoMotoEntity();

		if (controlDTO.getIdControlMoto() != null) {
			entity.setIdControlMoto(controlDTO.getIdControlMoto());
		}
		entity.setIdMoto(controlDTO.getIdMoto());
		entity.setKilometraje(controlDTO.getKilometraje());
		entity.setFecha(valid.fechaTimestamp(controlDTO.getFecha()));
		entity.setEvtidevento(controlDTO.getEvtidevento() != null ? controlDTO.getEvtidevento() : KL);
		entity.setEstadoControlMoto(controlDTO.getEstadoControlMoto() != null ? controlDTO.getEstadoControlMoto() : VL);

		return entity;

	}

	/**
	 * 
	 * @param KilometrajenotificacionEntity
	 * @return StatusResponse
	 */
	public ResponseKilometrajenotificacion insertUpdatetKilometrajenotificacion(KilometrajenotificacionEntity entity) {
		return service.insertUpdatetKilometrajenotificacion(entity);
	}

	/**
	 * 
	 * @param KilometrajenotificacionDTO
	 * @return KilometrajenotificacionEntity
	 */
	public KilometrajenotificacionEntity maperKilometrajenotificacionDTOToEntity(KilometrajenotificacionDTO kilNotDTO) {
		KilometrajenotificacionEntity entity = new KilometrajenotificacionEntity();

		if (kilNotDTO.getKnidkilometrajenoti() != null) {
			entity.setKnidkilometrajenoti(kilNotDTO.getKnidkilometrajenoti());
		}
		Timestamp timestamp2 = new Timestamp(new Date().getTime());
		entity.setNtfecha(timestamp2);
		entity.setMtidmoto(kilNotDTO.getMtidmoto());
		entity.setEvtidevento(kilNotDTO.getEvtidevento());
		entity.setKilometrajevalidador(kilNotDTO.getKilometrajevalidador());
		entity.setKntkilometraje(kilNotDTO.getKntkilometraje());
		entity.setStidestado(kilNotDTO.getStidestado() != null ? kilNotDTO.getStidestado() : VL);

		return entity;
	}

}

/**
 * 
 */
package com.wsgeomot.co.model.response;

import java.util.List;

import com.wsgeomot.co.model.dto.ControlEstadoMotoDTO;
import com.wsgeomot.co.model.dto.StatusResponse;

/**
 * @author RentAdvisor
 *
 */
public class ResponseContEstMotoDTOList {
	private List<ControlEstadoMotoDTO> controlEstadoMotoDTOList;
	private StatusResponse statusResponse;

	/**
	 * @return the controlEstadoMotoDTOList
	 */
	public List<ControlEstadoMotoDTO> getControlEstadoMotoDTOList() {
		return controlEstadoMotoDTOList;
	}

	/**
	 * @param controlEstadoMotoDTOList the controlEstadoMotoDTOList to set
	 */
	public void setControlEstadoMotoDTOList(List<ControlEstadoMotoDTO> controlEstadoMotoDTOList) {
		this.controlEstadoMotoDTOList = controlEstadoMotoDTOList;
	}

	/**
	 * @return the statusResponse
	 */
	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	/**
	 * @param statusResponse the statusResponse to set
	 */
	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}

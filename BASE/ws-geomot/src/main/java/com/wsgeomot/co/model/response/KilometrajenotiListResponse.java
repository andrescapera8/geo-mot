/**
 * 
 */
package com.wsgeomot.co.model.response;

import java.util.List;

import com.wsgeomot.co.model.dto.StatusResponse;

/**
 * @author Andres Capera
 *
 */
public class KilometrajenotiListResponse {
	private List<KilometrajenotiListResponse> kilometrajenotiListResponse;
	private StatusResponse statusResponse;
	/**
	 * @return the kilometrajenotiListResponse
	 */
	public List<KilometrajenotiListResponse> getKilometrajenotiListResponse() {
		return kilometrajenotiListResponse;
	}
	/**
	 * @param kilometrajenotiListResponse the kilometrajenotiListResponse to set
	 */
	public void setKilometrajenotiListResponse(List<KilometrajenotiListResponse> kilometrajenotiListResponse) {
		this.kilometrajenotiListResponse = kilometrajenotiListResponse;
	}
	/**
	 * @return the statusResponse
	 */
	public StatusResponse getStatusResponse() {
		return statusResponse;
	}
	/**
	 * @param statusResponse the statusResponse to set
	 */
	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}
	
	
}

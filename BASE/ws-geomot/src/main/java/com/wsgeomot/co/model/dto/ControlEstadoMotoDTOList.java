/**
 * 
 */
package com.wsgeomot.co.model.dto;

import java.util.List;

/**
 * @author Andres Capera
 *
 */
public class ControlEstadoMotoDTOList {
private List<ControlEstadoMotoDTO> controlEstadoMotoDTO;

/**
 * @return the controlEstadoMotoDTO
 */
public List<ControlEstadoMotoDTO> getControlEstadoMotoDTO() {
	return controlEstadoMotoDTO;
}

/**
 * @param controlEstadoMotoDTO the controlEstadoMotoDTO to set
 */
public void setControlEstadoMotoDTO(List<ControlEstadoMotoDTO> controlEstadoMotoDTO) {
	this.controlEstadoMotoDTO = controlEstadoMotoDTO;
}

}

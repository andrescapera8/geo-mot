/**
 * 
 */
package com.wsgeomot.co.model.response;

import com.wsgeomot.co.model.dto.ControlEstadoMotoDTO;
import com.wsgeomot.co.model.dto.StatusResponse;

/**
 * @author Andres Capera
 *
 */
public class ControlEstadoMotoDTOResponse {
	private ControlEstadoMotoDTO controlEstadoMotoDTO;
	private StatusResponse statusResponse;

	/**
	 * @return the controlEstadoMotoDTO
	 */
	public ControlEstadoMotoDTO getControlEstadoMotoDTO() {
		return controlEstadoMotoDTO;
	}

	/**
	 * @param controlEstadoMotoDTO the controlEstadoMotoDTO to set
	 */
	public void setControlEstadoMotoDTO(ControlEstadoMotoDTO controlEstadoMotoDTO) {
		this.controlEstadoMotoDTO = controlEstadoMotoDTO;
	}

	/**
	 * @return the statusResponse
	 */
	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	/**
	 * @param statusResponse the statusResponse to set
	 */
	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}

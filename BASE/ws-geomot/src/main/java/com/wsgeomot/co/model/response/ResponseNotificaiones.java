/**
 * 
 */
package com.wsgeomot.co.model.response;

import java.util.List;

import com.wsgeomot.co.model.dto.NotificaionesRS;
import com.wsgeomot.co.model.dto.StatusResponse;

/**
 * @author Andres Capera
 *
 */
public class ResponseNotificaiones {
	private List<NotificaionesRS> notificacionList;

	private StatusResponse statusResponse;

	/**
	 * @return the notificacionList
	 */
	public List<NotificaionesRS> getNotificacionList() {
		return notificacionList;
	}

	/**
	 * @param notificacionList the notificacionList to set
	 */
	public void setNotificacionList(List<NotificaionesRS> notificacionList) {
		this.notificacionList = notificacionList;
	}

	/**
	 * @return the statusResponse
	 */
	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	/**
	 * @param statusResponse the statusResponse to set
	 */
	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}

package com.wsgeomot.co.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ControlEstadoMotoDTO {

	private Integer idControlMoto;

	@Min(value = 1, message = "Min 1")
	@NotNull(message = "idMoto cannot be null")
	private Integer idMoto;

	private String fecha;

	@Min(value = 1, message = "Min 1")
	@NotNull(message = "kilometraje cannot be null")
	private Double kilometraje;

	@Min(value = 0, message = "Min 1")
	@NotNull(message = "evtidevento cannot be null")
	private Integer evtidevento;

	private String descripcion;

	private Integer estadoControlMoto;

	/**
	 * @return the idControlMoto
	 */
	public Integer getIdControlMoto() {
		return idControlMoto;
	}

	/**
	 * @param idControlMoto the idControlMoto to set
	 */
	public void setIdControlMoto(Integer idControlMoto) {
		this.idControlMoto = idControlMoto;
	}

	/**
	 * @return the idMoto
	 */
	public Integer getIdMoto() {
		return idMoto;
	}

	/**
	 * @param idMoto the idMoto to set
	 */
	public void setIdMoto(Integer idMoto) {
		this.idMoto = idMoto;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the kilometraje
	 */
	public Double getKilometraje() {
		return kilometraje;
	}

	/**
	 * @param kilometraje the kilometraje to set
	 */
	public void setKilometraje(Double kilometraje) {
		this.kilometraje = kilometraje;
	}

	/**
	 * @return the evtidevento
	 */
	public Integer getEvtidevento() {
		return evtidevento;
	}

	/**
	 * @param evtidevento the evtidevento to set
	 */
	public void setEvtidevento(Integer evtidevento) {
		this.evtidevento = evtidevento;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the estadoControlMoto
	 */
	public Integer getEstadoControlMoto() {
		return estadoControlMoto;
	}

	/**
	 * @param estadoControlMoto the estadoControlMoto to set
	 */
	public void setEstadoControlMoto(Integer estadoControlMoto) {
		this.estadoControlMoto = estadoControlMoto;
	}

}

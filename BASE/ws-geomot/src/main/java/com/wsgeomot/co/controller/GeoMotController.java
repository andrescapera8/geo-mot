package com.wsgeomot.co.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wsgeomot.co.model.dto.ContactoDTO;
import com.wsgeomot.co.model.dto.ControlEstadoMotoDTO;
import com.wsgeomot.co.model.dto.ControlEstadoMotoDTOList;
import com.wsgeomot.co.model.dto.KilometrajenotiListDTO;
import com.wsgeomot.co.model.dto.KilometrajenotificacionDTO;
import com.wsgeomot.co.model.dto.MotoDTO;
import com.wsgeomot.co.model.dto.PersonaContactoRequest;
import com.wsgeomot.co.model.dto.PersonaDTO;
import com.wsgeomot.co.model.dto.RequesInfoGeneral;
import com.wsgeomot.co.model.dto.StatusResponse;
import com.wsgeomot.co.model.response.ControlEstadoMotoDTOResponseGN;
import com.wsgeomot.co.model.response.ResponseContEstMotoDTOList;
import com.wsgeomot.co.model.response.ResponseContactoGL;
import com.wsgeomot.co.model.response.ResponseEventoPlantillaDTO;
import com.wsgeomot.co.model.response.ResponseKilometrajeNotiList;
import com.wsgeomot.co.model.response.ResponseKilometrajenotificacion;
import com.wsgeomot.co.model.response.ResponseMaestraLista;
import com.wsgeomot.co.model.response.ResponseMotoEntity;
import com.wsgeomot.co.model.response.ResponseNotificaiones;
import com.wsgeomot.co.model.response.ResponsePersonaEntity;
import com.wsgeomot.co.service.GeoMotService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin
@RestController
@RequestMapping(value = "v1/")
public class GeoMotController {

	@Autowired
	GeoMotService service;

	/**
	 * METODO DE CONSULTA DE INFORMACION GENERAL PERSONA MOTO Y NOTIFICACIONES
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return RequesInfoGeneral
	 */
	@Operation(summary = "METODO DE CONSULTA DE INFORMACION GENERAL")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "408", description = "ConnectionTimeout"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "204", description = "Data Not Found"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@GetMapping("/info-persona-moto")
	public RequesInfoGeneral getInfoMotoPersona(@RequestParam(value = "", required = true) Integer tipoDocumento,
			@RequestParam(value = "", required = true) String numDocumento,
			@RequestParam(value = "", required = false) String placa,
			@RequestParam(value = "", required = false) String param) {
		return service.getInfoMotoPersona(tipoDocumento, numDocumento, placa, param);
	}

	/**
	 * METODO DE CONSULTA DE LISTAS MAESTRAS
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return ResponseMaestraLista
	 */
	@Operation(summary = "METODO DE CONSULTA DE LISTAS MAESTRAS")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "408", description = "ConnectionTimeout"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "204", description = "Data Not Found"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@GetMapping("/listas-maestras")
	public ResponseMaestraLista getInfoListasMaestras(@RequestParam(value = "", required = true) String tipoDato) {
		return service.getInfoListasMaestras(tipoDato);
	}

	/**
	 * METODO DE CONSULTA DE EVENTOS Y PLANTILLAS
	 * 
	 * @param idEvento
	 * @returnResponseEventoPlantillaDTO
	 */
	@Operation(summary = "METODO DE CONSULTA DE EVENTOS Y PLANTILLAS")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "408", description = "ConnectionTimeout"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "204", description = "Data Not Found"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@GetMapping("/eventos-plantillas")
	public ResponseEventoPlantillaDTO getEventoPlantilla(@RequestParam(value = "", required = false) Integer idEvento) {
		return service.getEventoPlantilla(idEvento);
	}
	
	/**
	 * METODO DE CONSULTA DE NOTIFICAIONES
	 * 
	 * @param mtIdMoto
	 * @return ResponseNotificaiones
	 */
	@Operation(summary = "METODO DE CONSULTA DE NOTIFICACIONES")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "408", description = "ConnectionTimeout"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "204", description = "Data Not Found"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@GetMapping("/notificaiones")
	public ResponseNotificaiones getNotificaiones(@RequestParam(value = "", required = true) Integer mtIdMoto) {
		return service.getNotificaiones(mtIdMoto);
	}
	
	/**
	 * METODO DE CONSULTA DE RECORRIDOS
	 * 
	 * @param idMoto, idEvento
	 * @return ResponseContEstMotoDTOList
	 */
	@Operation(summary = "METODO DE CONSULTA DE RECORRIDOS")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "408", description = "ConnectionTimeout"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "204", description = "Data Not Found"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@GetMapping("/control-estado-moto")
	public ResponseContEstMotoDTOList getControlEstadoMoto(@RequestParam(value = "", required = true) Integer idMoto,
			@RequestParam(value = "", required = true) Integer idEvento) {
		return service.getControlEstadoMoto(idMoto, idEvento);
	}

	/**
	 * METODO DE REGISTRO DE PERSONA Y CONTACTO
	 * 
	 * @param persoContacRequest
	 * @return ResponseContactoGL
	 */
	@Operation(summary = "METODO DE REGISTRO DE PERSONA Y CONTACTO")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/persona-contacto")
	@ResponseBody
	public ResponseContactoGL insertUpdatePersonaContactoDTO(@RequestBody PersonaContactoRequest persoContacRequest) {
		return service.insertUpdatePersonaContactoDTO(persoContacRequest);
	}

	/**
	 * METODO DE RESGISTRO O ACTUALIZACION DE PERSONA
	 * 
	 * @param persona
	 * @return insertUpdatePersonaDTO
	 */
	@Operation(summary = "METODO DE RESGISTRO O ACTUALIZACION DE PERSONA")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/persona")
	@ResponseBody
	public ResponsePersonaEntity insertUpdatePersonaDTO(@RequestBody PersonaDTO persona) {
		return service.insertUpdatePersonaDTO(persona);
	}

	/**
	 * METODO DE REGISTRO O ACTUALIZACION CONTACTO DE PERSONA
	 * 
	 * @param contacto
	 * @return StatusResponse
	 */
	@Operation(summary = "METODO DE REGISTRO O ACTUALIZACION CONTACTO DE PERSONA")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/contacto")
	@ResponseBody
	public StatusResponse insertUpdateContactoDTO(@RequestBody ContactoDTO contacto) {
		return service.insertUpdateContactoDTO(contacto);
	}

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE MOTO
	 * 
	 * @param motoDTO
	 * @return StatusResponse
	 */
	@Operation(summary = "METODO DE REGISTRO O ACTUALIZACION DE MOTO")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/moto")
	@ResponseBody
	public ResponseMotoEntity insertUpdateMotoDTO(@RequestBody MotoDTO moto) {
		return service.insertUpdateMotoDTO(moto);
	}

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE ESTADO KILOMETRAJE MOTO
	 * 
	 * @param controlEstadoMoto
	 * @return StatusResponse
	 */
	@Operation(summary = "METODO DE REGISTRO O ACTUALIZACION DE ESTADO KILOMETRAJE MOTO")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/control-estado-moto")
	@ResponseBody
	public StatusResponse insertUpdateContEstadoMotoDTO(@RequestBody ControlEstadoMotoDTO controlEstadoMoto) {
		return service.insertUpdateContEstadoMotoDTO(controlEstadoMoto);
	}
	
	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE ESTADO KILOMETRAJE MOTO
	 * 
	 * @param controlEstadoMoto
	 * @return StatusResponse
	 */
	@Operation(summary = "METODO DE REGISTRO O ACTUALIZACION DE LISTA ESTADO KILOMETRAJE MOTO")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/control-estados-moto")
	@ResponseBody
	public ControlEstadoMotoDTOResponseGN insertUpdateContEstadoMotoDTOList(@RequestBody ControlEstadoMotoDTOList controlEstadoMotoList) {
		return service.insertUpdateContEstadoMotoDTOList(controlEstadoMotoList);
	}

	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO
	 * 
	 * @param kilometrajenotificacion
	 * @return StatusResponse
	 */
	@Operation(summary = "METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/kilometraje-notificacion-moto")
	@ResponseBody
	public ResponseKilometrajenotificacion insertUpdateKilometrajenotificacionDTO(
			@RequestBody KilometrajenotificacionDTO kilometrajenotificacion) {
		return service.insertUpdateKilometrajenotificacionDTO(kilometrajenotificacion);
	}

	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO LISTA
	 * 
	 * @param KilometrajenotiListDTO
	 * @return ResponseKilometrajeNotiList
	 */
	@Operation(summary = "METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO LISTA")
	@ApiResponses(value = { @ApiResponse(responseCode = "400", description = "Technical Error"),
			@ApiResponse(responseCode = "422", description = "Unprocessable Entity"),
			@ApiResponse(responseCode = "430", description = "Database Exception"),
			@ApiResponse(responseCode = "200", description = "Success", content = @Content) })
	@PostMapping(path = "/kilometrajes-notificaciones-moto")
	@ResponseBody
	public ResponseKilometrajeNotiList insertUpdateKilometrajenotificacionListDTO(
			@RequestBody KilometrajenotiListDTO dto) {
		return service.insertUpdateKilometrajenotificacionListDTO(dto);
	}

}

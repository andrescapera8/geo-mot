package com.wsgeomot.util;

import com.wsgeomot.co.model.dto.StatusResponse;

public class ResponseCodes {
	public static final StatusResponse SUCCESS = new StatusResponse("200", "Success");
	public static final StatusResponse TECHNICAL_ERROR = new StatusResponse("400", "Technical Error");
	public static final StatusResponse UNPROCESSABLE_ENTITY = new StatusResponse("422", "Unprocessable Entity");
	public static final StatusResponse NOT_FOUND = new StatusResponse("404", "Not Found");
	public static final StatusResponse DATA_NOT_FOUND = new StatusResponse("204", "Data Not Found");
	public static final StatusResponse TIMEOUT_EXCEPTION = new StatusResponse("408", "ConnectionTimeout");
	public static final StatusResponse DATABASE_EXCEPTION = new StatusResponse("430", "Database Exception");
	public static final StatusResponse REGISTERED_MOTORCYCLE = new StatusResponse("205", "Motorcycle registered by another user");
	public static final StatusResponse EXISTINGEVENT = new StatusResponse("206", "Existing Event");
	public static final StatusResponse PERSONAL_DATA_PAGE = new StatusResponse("1", "1");
	public static final StatusResponse VEHICLE_PAGE = new StatusResponse("2", "2");
	public static final StatusResponse EVENTS_PAGE = new StatusResponse("3", "3");
	public static final StatusResponse BLUETOOTH = new StatusResponse("4", "4");

	private ResponseCodes() {
		super();
	}
}

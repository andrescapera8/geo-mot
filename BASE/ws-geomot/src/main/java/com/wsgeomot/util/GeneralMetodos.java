/**
 * 
 */
package com.wsgeomot.util;

import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * @author Andres Capera
 *
 */
public class GeneralMetodos {

	private static final Logger logger = Logger.getLogger(GeneralMetodos.class);

	/**
	 * 
	 * @param value
	 * @return
	 */
	public String validString(String value) {
		String data = null;
		try {
			if (value!= null && !value.isEmpty() && !value.equals("")) {
				data = value.trim();
			}
			return data;
		} catch (Exception e) {
			String message = "GeneralMetodos validString" + e.getMessage() + e.getCause();
			logger.error(message);
			return data;
		}

	}

	public Timestamp fechaTimestamp(String value) {
		Date date = new Date();
		Timestamp timestamp2 = new Timestamp(date.getTime());
		try {
			return Timestamp.valueOf(value);
		} catch (Exception e) {
			String message = "GeneralMetodos validString" + e.getMessage() + e.getCause();
			logger.error(message);
			return timestamp2;
		}
	}
}




export class ServiceConfigDTO {
  knidkilometrajenoti: any; // ID kilometraje de notificacion
  evtidevento: any;
  mtidmoto: any;
  kilometrajevalidador: any; // Kilometraje de la revision del evento
  descripcin: any;
  stidestado: any = null;
  kntkilometraje: any; // Kilometraje de notificacion
  // ntduracion: any;
  // ntnumrepeticion: any;
  // nttiemporepeticion: any;
}
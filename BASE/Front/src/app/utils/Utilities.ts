


export class Utilities {

  // Return direcTo
  redirecTo(id: string) {
    let router: string = '';

    switch (id) {
      case '1':
        router = '/datos-personales';
        break;
      case '2':
        router = '/vehiculo';
        break;
      case '3':
        router = '/configuracion-servicio';
        break;
      case '4':
        router = '/bluetooth';
        break;
      default:
        break;
    }

    return router;
  }
}

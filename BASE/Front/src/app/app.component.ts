import { Component } from '@angular/core';
import { BackgroundMode } from '@awesome-cordova-plugins/background-mode/ngx';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { LoadingController, ToastController } from '@ionic/angular';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { takeUntil } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  ctrlPercent: boolean = true;

  private stopSubcription$ = new Subject<void>();


  constructor(
    public loadingBarService: LoadingBarService,
    private loadingCtrl: LoadingController,
    private backgroundMode: BackgroundMode,
    private bluetoothSerial: BluetoothSerial,
    private toastController: ToastController,
    private localNotifications: LocalNotifications
  ) { }

  ngOnInit() {
    this.loadingBarService.value$.subscribe(async (resp) => {
      let totalLoader = parseInt(resp.toString());

      if (totalLoader > 0 && this.ctrlPercent) {
        this.presentLoadingWithOptions();
        this.ctrlPercent = false;
      } else if (totalLoader == 0) {
        await this.closeLoader();
        this.ctrlPercent = true;
      }
    });

    this.backgroundMode.on("activate").subscribe(() => {
      this.bluetoothSerial.isConnected().then(res => {
        this.readBluetoothData();
      });
    });

    setTimeout(() => {
      this.callNotifyService();
    }, 60000);

    this.readBluetoothData();
    this.simpleNotif()
  }


  presentLoadingWithOptions() {
    this.loadingCtrl.create({
      spinner: 'bubbles',
      translucent: true,
      cssClass: 'custom-class custom-loading',
    }).then((res) => res.present());
  }

  closeLoader() {
    this.loadingCtrl.dismiss().then((res) => res).catch((error) => error);
  }

  readBluetoothData() {
    this.bluetoothSerial.read().then(buff => {
      this.showToast(buff);
    });
  }

  callNotifyService() {
    // this._srv.savePersonalInformation()
    //   .pipe(takeUntil(this.stopSubcription$))
    //   .subscribe((res: any) => {
    //     if (res.statusResponse.status == 200) {
    //       console.log('entraaaaaaaaaa')
    //     }
    //   });
  }

  simpleNotif() {
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification',
      data: { secret: 'secret' }
    });
  }


  async showToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000
    });

    toast.present();
  }
}

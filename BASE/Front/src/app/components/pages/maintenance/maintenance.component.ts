/* eslint-disable @typescript-eslint/consistent-type-assertions */
import { Component, OnInit } from '@angular/core';
import { ServiceConfigDTO } from '../../../models/service-config/ServiceConfigDTO';
import { ControlEstadoVehicle } from '../../../models/control-estado-vehicle/ControlEstadoVehicle';
import { Subject } from 'rxjs';
import { ControlEstadoVehicleService } from '../../../services/control-estado-vehicle/control-estado-vehicle.service';
import { takeUntil } from 'rxjs/operators';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['./maintenance.component.css']
})
export class MaintenanceComponent implements OnInit {
  controlEstadoMotoDTOList: ControlEstadoVehicle[] = [];
  pld: any = {};
  columnMode = ColumnMode;


  private stopSubcription$ = new Subject<void>();
  constructor(
    private _servContEstVehiculo: ControlEstadoVehicleService,
  ) { }

  ngOnInit(): void {
    this.pld = JSON.parse(localStorage.getItem('handler'));

    if (this.pld && this.pld?.vehicleId) {
      this._servContEstVehiculo.getControlEstadoVehiculo(this.pld?.vehicleId, 1)
        .pipe(takeUntil(this.stopSubcription$))
        .subscribe(res => {
          if (res.statusResponse.status == 200) {
            this.controlEstadoMotoDTOList = res.controlEstadoMotoDTOList;
             // eslint-disable-next-line max-len
             this.controlEstadoMotoDTOList = this.controlEstadoMotoDTOList.sort((a: any, b: any) => <any>new Date(b.fecha) - <any>new Date(a.fecha));;
          }

        });
    }
  }
}

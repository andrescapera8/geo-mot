import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController, ToastController } from '@ionic/angular';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MileageDTO } from '../../../models/service-config/MileageDTO';
import { ServiceConfigDTO } from '../../../models/service-config/ServiceConfigDTO';
import { RegisterPersonService } from '../../../services/register-person/register-person.service';
import { ServiceConfigService } from '../../../services/service-config/service-config.service';
import { ControlEstadoVehicleService } from './../../../services/control-estado-vehicle/control-estado-vehicle.service';
import { ListasMaestrasService } from './../../../services/listas-maestras/listas-maestras.service';


@Component({
  selector: 'app-service-config',
  templateUrl: './service-config.component.html',
  styleUrls: ['./service-config.component.scss'],
})
export class ServiceConfigComponent implements OnInit {

  @ViewChild('eventForm') private eventForm: NgForm;

  sendServiceConfig: ServiceConfigDTO[] = [];
  miliage: MileageDTO = new MileageDTO();

  event$: any;
  eventsTable$: any;

  pld: any = {};

  event: ServiceConfigDTO = new ServiceConfigDTO();
  events: ServiceConfigDTO[] = [];

  listaMaestraEventosDTO: any;
  listaMaestraNumEventosDTO: any;

  titleButt: string = 'Agregar';
  insertUpdate: boolean = false;


  index: number = null;


  private stopSubcription$ = new Subject<void>();


  constructor(
    private toastCtrl: ToastController,
    private alertController: AlertController,
    private _registerPerSrv: RegisterPersonService,
    private _servCongSrv: ServiceConfigService,
    private _listaMaestras: ListasMaestrasService,
    private _servContEstVehiculo: ControlEstadoVehicleService,
  ) { }

  ngOnInit() {
    this.pld = JSON.parse(localStorage.getItem('handler'));

    combineLatest(
      this._listaMaestras.getEvents(0),
      this._listaMaestras.getListasMaestras('CILINDRAJE'),
      this._registerPerSrv.getInfoPerson(this.pld, 'KILOMETRAJENOTI'),
      this._servContEstVehiculo.getControlEstadoVehiculo(this.pld?.vehicleId, 0)
    )
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(([Data1, Data2, Data3, Data4]) => {
        if (Data1.statusResponse.status == 200) {
          this.listaMaestraEventosDTO = Data1.eventoLista;
        }

        if (Data2.statusResponse.status == 200) {
          // this.listaMaestraNumEventosDTO = Data2.listaMaestraDTO;
        }

        if (Data3.statusResponse.status == 200) {
          this.events = Data3.kilometrajenotificacionDTOList;
        }

        if (Data4.statusResponse.status == 200 && Data4.controlEstadoMotoDTOList.length <= 0) this.currentMiliage(false)
      });
  }

  saveEvent() {
    if (this.event.kntkilometraje > this.event.kilometrajevalidador) {
      return this.openToast('El kilometraje de notificación es mayor')
    }

    this.addEvent(this.event)
  }

  addEvent(event) {
    event.mtidmoto = this.pld?.vehicleId;

    this._registerPerSrv.getInfoPerson(this.pld, 'FILTER_VEHICLE')
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          const pers = JSON.parse(localStorage.getItem('idUser'));

          if (resp.generalData.pers == pers || resp.generalData.pers == null) {
            this._servCongSrv.saveEvent(event)
              .pipe(takeUntil(this.stopSubcription$))
              .subscribe((resp: any) => {
                if (resp.statusResponse.status == 200) {
                  this.openToast('Información guardada correctamente.');
                  this.titleButt = 'Agregar';

                  this.getInfoPerson();
                } else if (resp.statusResponse.status == 206) {
                  this.presentAlert('El evento para registrar ya existe, por favor cambiar Kilometraje de notificación o revisión.');
                } else {
                  this.openToast('La información no fue guardada.');
                }
              });
          } else {
            this.presentAlert('Usuario no permitido. Ingresar con el usuario a guardar información.');
          }
        }
      });
  }

  getInfoPerson() {
    this._registerPerSrv.getInfoPerson(this.pld, 'KILOMETRAJENOTI')
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          this.events = resp.kilometrajenotificacionDTOList;
          const table = [...this.events];
          this.events = [];
          this.events = table;

          this.eventForm.resetForm();
        }
      });
  }

  registerMileage(miliage) {
    this.miliage.idMoto = this.pld?.vehicleId;
    this.miliage.kilometraje = miliage;

    this._servCongSrv.registerMileage(this.miliage)
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.status == 200) {
          this.openToast('Información guardada correctamente.');
        } else {
          this.openToast('Error tecnico en registro de Kilometraje.');
          this.currentMiliage(true);
        }
      });
  }

  editEvent(data) {
    this.event = { ...data };
    this.titleButt = 'Actualizar';
  }

  deleteEvn(data) {
    const event = data;
    event.stidestado = 2;

    this.addEvent(event);
  }

  attendEvent(index) {
    this.miliage = new MileageDTO();
    this.miliage.evtidevento = this.events[index].evtidevento;
    this.currentMiliage(true)
  }



  /**
   *  Cambia valores de los select
   */
  onChangeEvent(data) {
    this.event.evtidevento = data.value;
  }


  async openToast(message) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2500,
    });
    toast.present();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Aviso',
      message: message,
      buttons: ['OK'],
    });

    await alert.present();
  }

  async currentMiliage(state) {
    const alert = await this.alertController.create({
      header: 'Ingresar Kilometraje',
      subHeader: 'No olvidar ingresar primero el kilometraje actual ',
      // message: 'Revisar kilometraje en el identificador del vehiculo',
      backdropDismiss: false,
      cssClass: 'buttonCss',
      buttons: [
        {
          text: 'Cancelar',
          role: 'confirm',
          handler: () => { return state ? true : false; },
        },
        {
          text: 'Guardar',
          role: 'confirm',
          handler: (data) => {
            if (data.miliage <= 0) return false;
            this.registerMileage(data.miliage);
          },
        },
      ],
      inputs: [
        {
          name: 'miliage',
          type: 'number',
          placeholder: 'Kilometraje actual de vehiculo',
        },
      ],
    });

    await alert.present();
  }

}

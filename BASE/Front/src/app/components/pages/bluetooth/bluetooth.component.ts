import { Component, OnInit } from '@angular/core';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { AlertController, ToastController, ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-bluetooth',
  templateUrl: './bluetooth.component.html',
  styleUrls: ['./bluetooth.component.scss'],
})
export class BluetoothComponent implements OnInit {

  devices: any = [];

  toggleBluet: boolean = false;
  loading: boolean = false;
  loadingConn: boolean = false;


  constructor(
    private bluetoothSerial: BluetoothSerial,
    private alertController: AlertController,
    private toastController: ToastController,
    private actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.checkBluetooth();
    this.checkDevice();
    this.readDevice();
  }


  checkBluetooth() {
    this.bluetoothSerial.isEnabled().then(res => {
      this.listDevices();
      this.toggleBluet = true;
    }, error => {
      this.toggleBluet = false;
    })
  }

  checkDevice() {
    this.bluetoothSerial.isConnected().then(res => {
      this.showError(res);
    }, error => {
      console.log(error);
    })
  }

  callEvent(event) {
    if (event.detail.checked) {
      this.turnOnBluetooth();
    } else {
      this.cleanListDevices();
    }
  }

  turnOnBluetooth() {
    this.bluetoothSerial.enable().then(res => {
      this.listDevices();
    }, error => {
      this.showError(error);
    })
  }

  // turnOffBluetooth() {
  //   this.bluetoothSerial.disabled().then(res => {
  //     this.listDevices();
  //   }, error => {
  //     this.showError(error);
  //   })
  // }

  listDevices() {
    this.bluetoothSerial.list().then(res => {
      this.devices = res;
    }, error => {
      this.showError("No se ha podido conectar, algo ha fallado.");
    })
  }

  cleanListDevices() {
    this.devices = [];
  }

  connect(address) {
    this.loading = true;
    this.loadingConn = false;

    this.bluetoothSerial.connect(address).subscribe(success => {
      this.deviceConnected();
      this.loading = false;
      this.loadingConn = true;
      this.showToast("Conectado correctamente");
    }, error => {
      this.showError(error);
      this.loading = false;
    });
  }

  disconneted() {
    this.bluetoothSerial.disconnect();
    this.loadingConn = false;
    this.showToast("Dispositivo desconectado correctamente");
  }

  deviceConnected() {
    this.bluetoothSerial.subscribe("\n").subscribe(success => {
      this.showToast(success)
    }, error => {
      this.showError(error);
    });
  }

  readDevice() {
    this.bluetoothSerial.read().then(success => {
      this.showToast(success);
    }, error => {
      this.showToast(error);
    });
  }


  async showError(msg) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: [{
        text: 'Okay',
        handler: () => {
          console.log('okay')
        }
      }]
    })

    alert.present();
  }

  async showToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000
    });

    toast.present();
  }


  async disconnectedDevice(address) {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Desconectar',
        role: 'destructive',
        icon: 'unlink',
        data: {
          address: address
        },
        handler: () => {
          this.disconneted();
        }
      }]
    });

    await actionSheet.present();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { isPlatform, ToastController } from '@ionic/angular';
import { AuthenticationDTO } from '../../../models/auth/AuthenticationDTO';
import { AuthenticationService } from '../../../services/auth/authentication.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {

  auth: AuthenticationDTO = new AuthenticationDTO();


  constructor(
    private router: Router,
    private _authSrv: AuthenticationService,
    private toastController: ToastController,
  ) {

    if (!isPlatform('capacitor')) {
      GoogleAuth.initialize();
    }
  }

  ngOnInit() { }


  signIn() {
    this._authSrv.googleLogin()
      .then((user) => {
        if (user.authentication?.accessToken) {
          console.log('user:', user)
          localStorage.setItem('token', JSON.stringify(user.authentication.accessToken));
          localStorage.setItem('idUser', JSON.stringify(user.id));

          this.router.navigate(['/filtro-vehiculo']);
        } else {
          this.showToast(user);
        }
      });
  }




  async showToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000
    });

    toast.present();
  }
}

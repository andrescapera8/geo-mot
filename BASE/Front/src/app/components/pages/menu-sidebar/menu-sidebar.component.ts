import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from './../../../services/auth/authentication.service';


@Component({
  selector: 'app-menu-sidebar',
  templateUrl: './menu-sidebar.component.html',
  styleUrls: ['./menu-sidebar.component.scss'],
})
export class MenuSidebarComponent implements OnInit {

  menus: any = [
    { id: 0, icon: 'analytics-outline', title: 'Datos Personales', link: 'datos-personales' },
    { id: 1, icon: 'car-sport-outline', title: 'Mi Vehiculo', link: 'vehiculo' },
    { id: 2, icon: 'calendar', title: 'Configuracion Servicio', link: 'configuracion-servicio' },
    { id: 3, icon: 'bluetooth-outline', title: 'Bluetooth', link: 'bluetooth' },
    { id: 4, icon: 'construct', title: 'Mantenimientos', link: 'data-maintenance' },
    { id: 4, icon: 'clipboard', title: 'Recorridos', link: 'data-tours' },
  ];

  constructor(
    private menuCtrl: MenuController,
    private _authSrv: AuthenticationService,
    private router: Router,
  ) { }

  ngOnInit() { }

  signOut() {
    this._authSrv.googleLogout()
      .then(() => this.router.navigateByUrl('/login', { replaceUrl: true }) && this.menuCtrl.toggle());
  }

}

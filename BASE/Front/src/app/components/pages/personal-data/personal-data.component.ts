import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RegisterPersonalDataDTO } from '../../../models/register-data/RegisterPersonalDataDTO';
import { RegisterPersonService } from '../../../services/register-person/register-person.service';
import { ContactDataDTO } from './../../../models/register-data/ContactDataDTO';
import { PersonalDataDTO } from './../../../models/register-data/PersonalDataDTO';
import { ListasMaestrasService } from './../../../services/listas-maestras/listas-maestras.service';
import { format, parseISO } from 'date-fns';


@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss'],
})
export class PersonalDataComponent implements OnInit {

  @ViewChild('addContactForm') private addContactForm: NgForm;
  @ViewChild('popoverDatetime') popoverDatetime;

  sendRegister: RegisterPersonalDataDTO = new RegisterPersonalDataDTO();
  contacts: ContactDataDTO[] = [];
  contactInit: ContactDataDTO = new ContactDataDTO();
  regisPers: PersonalDataDTO = new PersonalDataDTO();

  submit: number = 0;
  index: number = null;

  pld: any = {};
  presentingElement = null;

  canDismiss: boolean = true;

  photoName: string = '';

  /**
   * LISTAS MAESTRAS MODELOS
   */
  listaMaestraGeneroDTO: any;
  listaMaestraTipoDocumDTO: any;
  listaMaestraGrupoSangDTO: any;
  listaMaestraContactoDTO: any;

  private stopSubcription$ = new Subject<void>();


  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private alertController: AlertController,
    private modalCtrl: ModalController,
    private registerPerSrv: RegisterPersonService,
    private _listaMaestras: ListasMaestrasService,
  ) { }

  ngOnInit() {
    this.presentingElement = document.querySelector('.ion-page');
    this.pld = JSON.parse(localStorage.getItem('handler'));

    combineLatest(
      this._listaMaestras.getListasMaestras('TIPOSEXO'),
      this._listaMaestras.getListasMaestras('TIPODOCUMENTO'),
      this._listaMaestras.getListasMaestras('TIPORH'),
      this.registerPerSrv.getInfoPerson(this.pld, 'PERSONA_CONTACTO'),
      this._listaMaestras.getListasMaestras('CONTACTO'),
    )
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(([Data1, Data2, Data3, Data4, Data5]) => {
        if (Data1.statusResponse.status == 200) {
          this.listaMaestraGeneroDTO = Data1.listaMaestraDTO;
        }

        if (Data2.statusResponse.status == 200) {
          this.listaMaestraTipoDocumDTO = Data2.listaMaestraDTO;
        }

        if (Data3.statusResponse.status == 200) {
          this.listaMaestraGrupoSangDTO = Data3.listaMaestraDTO;
        }

        if (Data4.statusResponse.status == 200) {
          this.regisPers = Data4.personaDTO;
          format(parseISO(this.regisPers.fechaNacimiento), 'MMM d, yyyy');
          this.contacts = Data4.contactoDTOList || [];
        }

        if (Data5.statusResponse.status == 200) {
          this.listaMaestraContactoDTO = Data5.listaMaestraDTO;
        }

        const dataRegPer = JSON.parse(localStorage.getItem('handler'));
        this.regisPers.numDocumento = dataRegPer.doc;
        this.regisPers.tipoDocumento = dataRegPer.type;
      });
  }


  saveRegister() {
    this.registerPerSrv.getInfoPerson(this.pld, 'FILTER_VEHICLE')
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          const pers = JSON.parse(localStorage.getItem('idUser'));

          if (resp.generalData.pers == pers || resp.generalData.pers == null) {
            this.registerPerSrv.savePersonalInformation(this.sendRegister)
              .pipe(takeUntil(this.stopSubcription$))
              .subscribe((res: any) => {
                if (res.statusResponse.status == 200) {
                  this.pld = {};
                  this.pld.type = res.persona.tipoDocumento;
                  this.pld.doc = res.persona.numDocumento;
                  this.pld.pers = res.persona.idPersona;

                  localStorage.setItem('handler', JSON.stringify(this.pld));
                  this.router.navigate(['/vehiculo']);
                }
              });
          } else {
            this.presentAlert('Usuario no permitido. Ingresar con el usuario a guardar información.');
          }
        }
      });
  }

  /**
   *  Adjunta archivo de fotografia en base64
   */
  onFileSelected(event) {
    const file = event.target.files[0];
    this.photoName = file.name;
    const reader = new FileReader();

    reader.onload = (e: any) => {
      this.regisPers.foto = e.target.result;
    };

    reader.readAsDataURL(file);
  }

  register() {
    if (this.contacts.length > 0) {
      this.regisPers.idPersona = JSON.parse(localStorage.getItem('idUser'));

      this.sendRegister.personaDTO = this.regisPers;
      this.sendRegister.contactoDTOList = this.contacts;

      this.saveRegister();
    } else {
      this.openToast('Debe agregar personas de contacto para registro.');
    }
  }

  addContacts() {
    const table = this.contacts;
    this.contacts = [];

    if (this.index) {
      table[this.index] = { ...this.contactInit };
      this.index = null;
      this.contactInit = new ContactDataDTO();
    } else {
      table.push({ ...this.contactInit })
    }

    this.contacts = [...table];
    this.addContactForm.resetForm();
  }

  deleteContact(index) {
    this.contacts.splice(index, 1);
    const table = [...this.contacts];
    this.contacts = [];

    this.contacts = table;
  }

  editContact(data, index) {
    this.index = index;
    this.contactInit = { ...data };
  }

  sendContData() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  /**
   *  Cambia valores de los select
   */
  onChangeTypeDoc(data) {
    this.regisPers.tipoDocumento = data.value;
  }

  onChangeGender(data) {
    this.regisPers.sexo = data.value;
  }

  onChangeRH(data) {
    this.regisPers.tipoRh = data.value;
  }

  onChangeTypeCont(data) {
    this.contactInit.tipocontacto = data.value.id;
    this.contactInit.descripcin = data.value.descripcin;
  }



  async openToast(message) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2500,
    });
    toast.present();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Aviso',
      message: message,
      buttons: ['OK'],
    });

    await alert.present();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FilterVehicle } from './../../../models/filter-vehicle/FilterVehicle';
import { ListasMaestrasService } from './../../../services/listas-maestras/listas-maestras.service';
import { RegisterPersonService } from './../../../services/register-person/register-person.service';
import { Utilities } from './../../../utils/Utilities';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';


@Component({
  selector: 'app-filter-vehicle',
  templateUrl: './filter-vehicle.component.html',
  styleUrls: ['./filter-vehicle.component.scss']
})
export class FilterVehicleComponent implements OnInit {

  filterVehicle: FilterVehicle = new FilterVehicle();

  listaMaestraTipoDocumDTO: any;

  private stopSubcription$ = new Subject<void>();


  constructor(
    private _listaMaestras: ListasMaestrasService,
    private registerPerSrv: RegisterPersonService,
    private router: Router,
    private util: Utilities,
    private localNotifications: LocalNotifications
  ) { }

  ngOnInit(): void {
    combineLatest(
      this._listaMaestras.getListasMaestras('TIPODOCUMENTO'),
    )
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(([Data1]) => {
        if (Data1.statusResponse.status == 200) {
          this.listaMaestraTipoDocumDTO = Data1.listaMaestraDTO;
        }
      });
  }


  getInfo() {
    const payload = {
      type: this.filterVehicle.tipoDocumento,
      doc: this.filterVehicle.document,
      placa: this.filterVehicle.placa
    }

    this.registerPerSrv.getInfoPerson(payload, 'FILTER_VEHICLE')
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          localStorage.setItem('handler', JSON.stringify(resp.generalData));

          this.router.navigate([this.util.redirecTo(resp.screen)]);
        }
      });
  }


  /**
   *  Cambia valores de los select
   */
  onChangeTypeDoc(data) {
    this.filterVehicle.tipoDocumento = data.value;
  }



  simpleNotif() {
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification',
      data: { secret: 'secret' }
    });
  }
}

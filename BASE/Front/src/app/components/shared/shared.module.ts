import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToolbarComponent } from './toolbar/toolbar.component';


@NgModule({
  declarations: [
    ToolbarComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    IonicModule
  ],
  exports: [
    ToolbarComponent,
    NgxDatatableModule,
  ]
})
export class SharedModule { }

import { Component, Input, OnInit } from '@angular/core';

const TOKEN_KEY = 'my-token';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

  @Input() $title: any;
  @Input() $icon: any;

  constructor() { }

  ngOnInit() { }

}

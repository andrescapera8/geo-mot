import { AuthGuard } from './guards/authGuard.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './components/pages/auth/auth.component';
import { BluetoothComponent } from './components/pages/bluetooth/bluetooth.component';
import { FilterVehicleComponent } from './components/pages/filter-vehicle/filter-vehicle.component';
import { PersonalDataComponent } from './components/pages/personal-data/personal-data.component';
import { ServiceConfigComponent } from './components/pages/service-config/service-config.component';
import { VehicleDataComponent } from './components/pages/vehicle-data/vehicle-data.component';
import { LoginGuard } from './guards/loginGuard.guard';
import { MaintenanceComponent } from './components/pages/maintenance/maintenance.component';
import { ToursComponent } from './components/pages/tours/tours.component';


const routes: Routes = [
  {
    path: 'login',
    component: AuthComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'filtro-vehiculo',
    component: FilterVehicleComponent,
    canActivate: []
  },
  {
    path: 'datos-personales',
    component: PersonalDataComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'vehiculo',
    component: VehicleDataComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'configuracion-servicio',
    component: ServiceConfigComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'bluetooth',
    component: BluetoothComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'data-maintenance',
    component: MaintenanceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'data-tours',
    component: ToursComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

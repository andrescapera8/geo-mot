import { Injectable } from '@angular/core';

import { ApiManagerService } from './../../common/api.manager.service';
import { environment } from './../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RegisterPersonService {

  constructor(
    private _api: ApiManagerService,
  ) { }

  /**
   *  Guarda la información del usuario a registrar
   */
  savePersonalInformation(payload) {
    const endpoint = environment.savePersonalData;

    return this._api.post(endpoint, payload)
  }

  /**
   *  Guarda la información del usuario a registrar
   */
  getInfoPerson(data, param) {
    let endpoint = environment.getPersonID + `?tipoDocumento=${data?.type}&numDocumento=${data?.doc}&param=${param}`;

    if (data.placa) endpoint += `&placa=${data?.placa}`

    return this._api.get(endpoint)
  }
}

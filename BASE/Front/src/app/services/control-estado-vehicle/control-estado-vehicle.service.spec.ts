import { TestBed } from '@angular/core/testing';

import { ControlEstadoVehicleService } from './control-estado-vehicle.service';

describe('ControlEstadoVehicleService', () => {
  let service: ControlEstadoVehicleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ControlEstadoVehicleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { ApiManagerService } from '../../common/api.manager.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ControlEstadoVehicleService {

  constructor(
    private _api: ApiManagerService,
  ) { }

  /**
  *  Listas del sistema
  */
  getControlEstadoVehiculo(idMoto, idEvento) {
    const endpoint = environment.controlEstadoVehiculo + '?idMoto=' + idMoto + '&idEvento=' + idEvento;
    return this._api.get(endpoint);
  }
}

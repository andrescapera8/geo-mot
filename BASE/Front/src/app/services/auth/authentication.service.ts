import { Injectable } from '@angular/core';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
  ) { }

  /**
   *  Funcion que realiza login con Google
   */
  googleLogin() {
    return GoogleAuth.signIn()
      .then(res => {
        this.isAuthenticated.next(true);
        return res
      })
      .catch(err => {
        this.isAuthenticated.next(true);
        return err
      });
  }

  /**
   *  Funcion que realiza logout con Google
   */
  googleLogout() {
    return GoogleAuth.signOut()
      .then(res => {
        this.isAuthenticated.next(false);
        return res
      })
      .catch(err => {
        this.isAuthenticated.next(false);
        return err
      });
  }

  public get getAuthentication() {
    return this.isAuthenticated.value;
  }
}

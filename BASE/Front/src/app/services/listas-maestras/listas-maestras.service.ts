import { Injectable } from '@angular/core';
import { ApiManagerService } from '../../common/api.manager.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListasMaestrasService {

  constructor(
    private _api: ApiManagerService,
  ) { }


  /**
   *  Listas del sistema
   */
  getListasMaestras(parameter) {
    const endpoint = environment.listasMaestras + '?tipoDato=' + parameter;
    return this._api.get(endpoint);
  }

  /**
   *  Eventos del sistema
   */
  getEvents(parameter) {
    const endpoint = environment.eventsMaestras + '?idEvento=' + parameter;
    return this._api.get(endpoint);
  }
}

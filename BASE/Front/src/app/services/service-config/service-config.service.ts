import { Injectable } from '@angular/core';

import { ApiManagerService } from '../../common/api.manager.service';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ServiceConfigService {

  constructor(
    private _api: ApiManagerService,
  ) { }


  /**
   *  Guarda la información del usuario a registrar
   */
  saveEvents(payload) {
    const endpoint = environment.saveEvents;

    return this._api.post(endpoint, payload);
  }

  saveEvent(payload) {
    const endpoint = environment.saveEvent;

    return this._api.post(endpoint, payload);
  }

  registerMileage(payload) {
    const endpoint = environment.regMiliag;

    return this._api.post(endpoint, payload);
  }
}

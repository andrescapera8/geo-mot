import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { AuthenticationService } from '../services/auth/authentication.service';
import { RegisterPersonService } from '../services/register-person/register-person.service';
import { Utilities } from '../utils/Utilities';


@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  private loggedIn: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private registerPerSrv: RegisterPersonService,
    private util: Utilities,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // if (this._autheSrv.getAuthentication) {
    //   this.router.navigate(['/bluetooth']);

    //   return false;
    // }

    // return this.checkAccess();
    return true;
  }

  checkAccess() {
    const payload = JSON.parse(localStorage.getItem('handler'));

    return this.registerPerSrv.getInfoPerson(payload, 'FILTER_VEHICLE').pipe(
      map(res => {
        if (res.statusResponse.status == 200) {
          if (res.screen == 4) {
            return true
          } else {
            this.router.navigateByUrl(this.util.redirecTo(res.screen));
            return false;
          }
        } else {
          return false;
        }
      })
    );
  }
}
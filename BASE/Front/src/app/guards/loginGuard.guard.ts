import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth/authentication.service';


@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {

  constructor(
    private router: Router,
    private _autheSrv: AuthenticationService
  ) { }

  canActivate(): boolean {
    if (this._autheSrv.getAuthentication) {
      this.router.navigate(['/bluetooth']);

      return false;
    }

    return true;
  }
}
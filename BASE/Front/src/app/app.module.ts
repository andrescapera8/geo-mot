import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiManagerService } from './common/api.manager.service';
import { FooterComponent } from './components/footer/footer.component';
import { AuthComponent } from './components/pages/auth/auth.component';
import { BluetoothComponent } from './components/pages/bluetooth/bluetooth.component';
import { MenuSidebarComponent } from './components/pages/menu-sidebar/menu-sidebar.component';
import { PersonalDataComponent } from './components/pages/personal-data/personal-data.component';
import { ServiceConfigComponent } from './components/pages/service-config/service-config.component';
import { VehicleDataComponent } from './components/pages/vehicle-data/vehicle-data.component';
import { SharedModule } from './components/shared/shared.module';
import { InterceptedService } from './interceptor/intercepted.service';
import { HttpHandlerService } from './utils/HttpHandler.service';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { BackgroundMode } from '@awesome-cordova-plugins/background-mode/ngx';
import { FilterVehicleComponent } from './components/pages/filter-vehicle/filter-vehicle.component';
import { Utilities } from './utils/Utilities';
import { MaintenanceComponent } from './components/pages/maintenance/maintenance.component';
import { ToursComponent } from './components/pages/tours/tours.component';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    PersonalDataComponent,
    VehicleDataComponent,
    ServiceConfigComponent,
    AuthComponent,
    MenuSidebarComponent,
    BluetoothComponent,
    FilterVehicleComponent,
    MaintenanceComponent,
    ToursComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
    }),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    LoadingBarHttpClientModule,
    IonicModule.forRoot(),
    NgxDatatableModule,
  ],
  entryComponents: [],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptedService, multi: true },
    ApiManagerService,
    Utilities,
    HttpHandlerService,
    BackgroundMode,
    BluetoothSerial,
    LocalNotifications
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// const URI = 'http://localhost:6868/geo-mot/v1';
const URI = 'https://ws-geo-mot.azurewebsites.net/v1';
// const URI = '';

export const environment = {
  production: false,


  savePersonalData: `${URI}/persona-contacto`,
  getPersonID: `${URI}/info-persona-moto`,

  saveVehicleData: `${URI}/moto`,

  listasMaestras: `${URI}/listas-maestras`,
  eventsMaestras: `${URI}/eventos-plantillas`,
  saveEvents: `${URI}/kilometrajes-notificaciones-moto`,
  saveEvent: `${URI}/kilometraje-notificacion-moto`,
  regMiliag: `${URI}/control-estado-moto`,
  controlEstadoVehiculo: `${URI}/control-estado-moto`,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

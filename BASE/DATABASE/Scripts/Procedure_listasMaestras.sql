CREATE DEFINER=`geomot`@`%` PROCEDURE `listasMaestras`(IN tipoDato VARCHAR(100), OUT data VARCHAR(5000))
BEGIN
	IF tipoDato = "COMBUSTILBE" THEN 
		SELECT JSON_OBJECT('listaMaestraDTO',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', cbeidcombustilbe,
		'siglas', siglas,
		'descripcin', descripcin,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.combustible WHERE stidestado = 1)) INTO data;
    END IF;
    
	IF tipoDato = "CILINDRAJE" THEN
		SELECT JSON_OBJECT('listaMaestraDTO',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', cleidcilindraje,
		'siglas', siglas,
		'descripcin', descripcin,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.cilindraje WHERE stidestado = 1)) INTO data;
	END IF;
    
	IF tipoDato = "COLOR" THEN
		SELECT JSON_OBJECT('listaMaestraDTO',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', clidcolor,
		'siglas', siglas,
		'descripcin', descripcin,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.color WHERE stidestado = 1)) INTO data;
	END IF;
    
	IF tipoDato = "CONTACTO" THEN
		SELECT JSON_OBJECT('listaMaestraDTO',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', ctoidcontacto,
		'siglas', siglas,
		'descripcin', descripcin,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.tipo_contacto WHERE stidestado = 1)) INTO data;  
	END IF;
    
    IF tipoDato = "MARCA" THEN
		SELECT JSON_OBJECT('listaMaestraDTO',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', maidmarca,
		'siglas', siglas,
		'descripcin', descripcin,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.marca WHERE stidestado = 1)) INTO data;
	END IF;
    
    IF tipoDato = "TIPODOCUMENTO" THEN
			SELECT JSON_OBJECT('listaMaestraDTO',
			(SELECT JSON_ARRAYAGG(
			JSON_OBJECT(
			'id', tdcidtipodocumento,
			'siglas', siglas,
			'descripcin', descripcin,
			'fechaRegistro', fechaRegistro,
			'estadoCombustible', stidestado
			)
			) FROM geomot.tipo_documento WHERE stidestado = 1)) INTO data;
	END IF;
    
    IF tipoDato = "TIPORH" THEN
		SELECT JSON_OBJECT('listaMaestraDTO',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', trhidtiporh,
		'siglas', siglas,
		'descripcin', descripcin,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.tipo_rh WHERE stidestado = 1)) INTO data;
	END IF;
    
    IF tipoDato = "TIPOSEXO" THEN
		SELECT JSON_OBJECT('listaMaestraDTO',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', sxidtiposexo,
		'siglas', siglas,
		'descripcin', descripcin,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.tipo_sexo WHERE stidestado = 1)) INTO data;
    END IF;
END
CREATE DEFINER=`geomot`@`%` PROCEDURE `getInfoMotoPersona`(IN tipoDocument NUMERIC(2), IN numDocument VARCHAR(12) , IN numPlaca VARCHAR(12), OUT data TEXT)
BEGIN
DECLARE var_idPersona INT DEFAULT 0;
DECLARE var_idMoto INT DEFAULT 0;
IF  numPlaca IS NOT NULL THEN
	SELECT PER.peridpersona INTO var_idPersona
	FROM geomot.persona AS PER LEFT JOIN geomot.moto AS MTO
	ON MTO.peridpersona = PER.peridpersona
	WHERE PER.tdcidtipodocumento = tipoDocument AND PER.pernumdocumento = numDocument AND MTO.mtplaca = numPlaca
	AND PER.stidestado = 1 AND MTO.stidestado = 1;

	SELECT MTO.mtidmoto INTO var_idMoto
	FROM geomot.persona AS PER INNER JOIN geomot.moto AS MTO
	ON MTO.peridpersona = PER.peridpersona
	WHERE PER.tdcidtipodocumento = tipoDocument AND PER.pernumdocumento = numDocument AND MTO.mtplaca = numPlaca
	AND PER.stidestado = 1 AND MTO.stidestado = 1;
    ELSE 
        SELECT PER.peridpersona INTO var_idPersona
		FROM geomot.persona AS PER 
		WHERE PER.tdcidtipodocumento = tipoDocument AND PER.pernumdocumento = numDocument
		AND PER.stidestado = 1;
END IF;
IF var_idPersona > 0 THEN
SELECT JSON_OBJECT('personaDTO', 
(SELECT JSON_OBJECT(
'idPersona', peridpersona,
'tipoDocumento', tdcidtipodocumento,
'numDocumento', pernumdocumento,
'nombres', pernombres,
'apellidos', perapellidos,
'sexo', sxidtiposexo,
'firmaDigital', convert(perfirmadigital using utf8),
'foto', convert(perfoto using utf8),
'fechaNacimiento', perfechanacimiento,
'estatura', perestatura,
'peso', perpeso,
'tipoRh', trhidtiporh,
'estadoPersona', stidestado
)FROM geomot.persona WHERE peridpersona = var_idPersona AND stidestado = 1),

'contactoDTOList',(
SELECT JSON_ARRAYAGG(
JSON_OBJECT(
'idContacto', ctoidcontacto,
'idPersona', peridpersona,
'tipocontacto', ctotipocontacto,
'valor', ctovalor,
'estadoContacto', stidestado
)
) FROM geomot.contacto WHERE peridpersona = var_idPersona AND stidestado = 1),

'motoDTO',(
SELECT JSON_OBJECT('idMoto', mtidmoto,
'idPersona', peridpersona,
'referencia', mtclase,
'clase', mtclase,
'cleidcilindraje', cleidcilindraje,
'marca', maidmarca,
'modelo', mtmodelo,
'placa', mtplaca,
'combustilbe', cbeidcombustilbe,
'numMotor', mtnummotor,
'numChasis', mtnumchasis,
'vin', mtvin,
'linea', mtlinea,
'color', clidcolor,
'peso', mtpeso,
'estadoMoto', stidestado)
FROM geomot.moto WHERE peridpersona = var_idPersona  AND mtidmoto = var_idMoto AND stidestado = 1
),

'kilometrajenotificacionDTOList', (
SELECT JSON_ARRAYAGG(
JSON_OBJECT(
'knidkilometrajenoti',  knidkilometrajenoti,
'mtidmoto',  mtidmoto,
'evtidevento',  evtidevento,
'kntkilometraje',  kntkilometraje,
'ntfecha',  ntfecha,
'ntduracion',  ntduracion,
'ntnumrepeticion',  ntnumrepeticion,
'nttiemporepeticion',  nttiemporepeticion,
'stidestado',  stidestado
)
) FROM geomot.kilometrajenotificacion WHERE mtidmoto = var_idMoto AND stidestado = 1
)
) INTO data;
END IF;
END
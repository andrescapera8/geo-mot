CREATE DEFINER=`geomot`@`%` PROCEDURE `getEventoPlantilla`(IN idEvento NUMERIC(2), OUT data VARCHAR(5000))
BEGIN
   IF (idEvento = 0) OR (idEvento IS NULL) THEN
		SELECT JSON_OBJECT('eventoLista',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'id', evtidevento,
		'siglas', siglas,
		'descripcin', descripcion,
		'fechaRegistro', fechaRegistro,
		'estadoCombustible', stidestado
		)
		) FROM geomot.tipo_evento WHERE stidestado = 1),
		'plantillaLista',
		(SELECT JSON_ARRAYAGG(
		JSON_OBJECT(
		'idplantillanoti',  idplantillanoti,
		'evtidevento',  evtidevento,
		'fechaPlantilla',  fechaPlantilla,
		'texto',  texto,
		'stidestado',  stidestado
		)) FROM geomot.plantillanotificacion WHERE stidestado = 1)
        )  INTO data;
        
        ELSE
				SELECT JSON_OBJECT('eventoLista',
				(SELECT JSON_ARRAYAGG(
				JSON_OBJECT(
				'id', evtidevento,
				'siglas', siglas,
				'descripcin', descripcion,
				'fechaRegistro', fechaRegistro,
				'estadoCombustible', stidestado
				)
				) FROM geomot.tipo_evento WHERE stidestado = 1 AND evtidevento = idEvento),
				'plantillaLista',
				(SELECT JSON_ARRAYAGG(
				JSON_OBJECT(
				'idplantillanoti',  idplantillanoti,
				'evtidevento',  evtidevento,
				'fechaPlantilla',  fechaPlantilla,
				'texto',  texto,
				'stidestado',  stidestado
				)) FROM geomot.plantillanotificacion WHERE stidestado = 1 AND evtidevento = idEvento)
				)  INTO data;
	END IF;
END
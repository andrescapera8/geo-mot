/**
 * 
 */
package com.wsgeomot.co.model.response;

import java.util.List;

import com.wsgeomot.co.model.dto.StatusResponse;

/**
 * @author Andres Capera
 *
 */
public class ControlEstadoMotoDTOResponseGN {
	private List<ControlEstadoMotoDTOResponse> controlEstadoMotoDTOResponse;
	private ResponseNotificaiones responseNotificaiones;
	private StatusResponse statusResponse;

	/**
	 * @return the controlEstadoMotoDTOResponse
	 */
	public List<ControlEstadoMotoDTOResponse> getControlEstadoMotoDTOResponse() {
		return controlEstadoMotoDTOResponse;
	}

	/**
	 * @param controlEstadoMotoDTOResponse the controlEstadoMotoDTOResponse to set
	 */
	public void setControlEstadoMotoDTOResponse(List<ControlEstadoMotoDTOResponse> controlEstadoMotoDTOResponse) {
		this.controlEstadoMotoDTOResponse = controlEstadoMotoDTOResponse;
	}

	/**
	 * @return the responseNotificaiones
	 */
	public ResponseNotificaiones getResponseNotificaiones() {
		return responseNotificaiones;
	}

	/**
	 * @param responseNotificaiones the responseNotificaiones to set
	 */
	public void setResponseNotificaiones(ResponseNotificaiones responseNotificaiones) {
		this.responseNotificaiones = responseNotificaiones;
	}

	/**
	 * @return the statusResponse
	 */
	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	/**
	 * @param statusResponse the statusResponse to set
	 */
	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}

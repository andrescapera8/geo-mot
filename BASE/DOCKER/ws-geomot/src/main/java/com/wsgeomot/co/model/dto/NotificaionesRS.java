/**
 * 
 */
package com.wsgeomot.co.model.dto;

/**
 * @author Andres Capera
 *
 */
public class NotificaionesRS {

	private Integer dmtidmoto;
	private Integer devtidevento;
	private Double dkntkilometraje;
	private Double dnttkilometrajevalidador;
	private String dntfecha;
	private Double dntduracion;
	private Double dkntkilometrajeSum;

	/**
	 * @return the dmtidmoto
	 */
	public Integer getDmtidmoto() {
		return dmtidmoto;
	}

	/**
	 * @param dmtidmoto the dmtidmoto to set
	 */
	public void setDmtidmoto(Integer dmtidmoto) {
		this.dmtidmoto = dmtidmoto;
	}

	/**
	 * @return the devtidevento
	 */
	public Integer getDevtidevento() {
		return devtidevento;
	}

	/**
	 * @param devtidevento the devtidevento to set
	 */
	public void setDevtidevento(Integer devtidevento) {
		this.devtidevento = devtidevento;
	}

	/**
	 * @return the dkntkilometraje
	 */
	public Double getDkntkilometraje() {
		return dkntkilometraje;
	}

	/**
	 * @param dkntkilometraje the dkntkilometraje to set
	 */
	public void setDkntkilometraje(Double dkntkilometraje) {
		this.dkntkilometraje = dkntkilometraje;
	}

	/**
	 * @return the dnttkilometrajevalidador
	 */
	public Double getDnttkilometrajevalidador() {
		return dnttkilometrajevalidador;
	}

	/**
	 * @param dnttkilometrajevalidador the dnttkilometrajevalidador to set
	 */
	public void setDnttkilometrajevalidador(Double dnttkilometrajevalidador) {
		this.dnttkilometrajevalidador = dnttkilometrajevalidador;
	}

	/**
	 * @return the dntfecha
	 */
	public String getDntfecha() {
		return dntfecha;
	}

	/**
	 * @param dntfecha the dntfecha to set
	 */
	public void setDntfecha(String dntfecha) {
		this.dntfecha = dntfecha;
	}

	/**
	 * @return the dntduracion
	 */
	public Double getDntduracion() {
		return dntduracion;
	}

	/**
	 * @param dntduracion the dntduracion to set
	 */
	public void setDntduracion(Double dntduracion) {
		this.dntduracion = dntduracion;
	}

	/**
	 * @return the dkntkilometrajeSum
	 */
	public Double getDkntkilometrajeSum() {
		return dkntkilometrajeSum;
	}

	/**
	 * @param dkntkilometrajeSum the dkntkilometrajeSum to set
	 */
	public void setDkntkilometrajeSum(Double dkntkilometrajeSum) {
		this.dkntkilometrajeSum = dkntkilometrajeSum;
	}

}

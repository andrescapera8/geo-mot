package com.wsgeomot.co.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class MotoDTO {
	private Integer idMoto;
	
	@Min(value = 1, message = "Min 8")
	@NotNull(message = "tipoDocumento cannot be null")
	private String  idPersona;
	
	private String referencia;
	
	private String clase;
	
	@Min(value = 1, message = "Min 1")
	@NotNull(message = "marca cannot be null")
	private Integer marca;
	
	@Min(value = 1, message = "Min 1")
	@NotNull(message = "cleidcilindraje cannot be null")
	private Integer cleidcilindraje;
	
	@NotNull(message = "modelo cannot be null")
	private String modelo;
	
	@NotNull(message = "placa cannot be null")
	private String placa;
	
	@Min(value = 1, message = "Min 1")
	@NotNull(message = "combustilbe cannot be null")
	private Integer combustilbe;
	
	private String numMotor;
	private String numChasis;
	private String vin;
	private String linea;
	
	@Min(value = 1, message = "Min 1")
	@NotNull(message = "color cannot be null")
	private Integer color;
	
	private Double peso;
	private Integer estadoMoto;

	/**
	 * @return the idMoto
	 */
	public Integer getIdMoto() {
		return idMoto;
	}

	/**
	 * @param idMoto the idMoto to set
	 */
	public void setIdMoto(Integer idMoto) {
		this.idMoto = idMoto;
	}

	/**
	 * @return the idPersona
	 */
	public String  getIdPersona() {
		return idPersona;
	}

	/**
	 * @param idPersona the idPersona to set
	 */
	public void setIdPersona(String  idPersona) {
		this.idPersona = idPersona;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public Integer getMarca() {
		return marca;
	}

	public void setMarca(Integer marca) {
		this.marca = marca;
	}

	/**
	 * @return the cleidcilindraje
	 */
	public Integer getCleidcilindraje() {
		return cleidcilindraje;
	}

	/**
	 * @param cleidcilindraje the cleidcilindraje to set
	 */
	public void setCleidcilindraje(Integer cleidcilindraje) {
		this.cleidcilindraje = cleidcilindraje;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getCombustilbe() {
		return combustilbe;
	}

	public void setCombustilbe(Integer combustilbe) {
		this.combustilbe = combustilbe;
	}

	public String getNumMotor() {
		return numMotor;
	}

	public void setNumMotor(String numMotor) {
		this.numMotor = numMotor;
	}

	public String getNumChasis() {
		return numChasis;
	}

	public void setNumChasis(String numChasis) {
		this.numChasis = numChasis;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Integer getEstadoMoto() {
		return estadoMoto;
	}

	public void setEstadoMoto(Integer estadoMoto) {
		this.estadoMoto = estadoMoto;
	}
}

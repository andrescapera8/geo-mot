/**
 * 
 */
package com.wsgeomot.co.model.dto;

import java.util.List;

/**
 * @author Andres Capera
 *
 */
public class KilometrajenotiListDTO {
	private List<KilometrajenotificacionDTO> kilometrajenotificacionListDTO;

	/**
	 * @return the kilometrajenotificacionListDTO
	 */
	public List<KilometrajenotificacionDTO> getKilometrajenotificacionListDTO() {
		return kilometrajenotificacionListDTO;
	}

	/**
	 * @param kilometrajenotificacionListDTO the kilometrajenotificacionListDTO to
	 *                                       set
	 */
	public void setKilometrajenotificacionListDTO(List<KilometrajenotificacionDTO> kilometrajenotificacionListDTO) {
		this.kilometrajenotificacionListDTO = kilometrajenotificacionListDTO;
	}

}

package com.wsgeomot.co.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ContactoDTO {

	private Integer idContacto;

	@Min(value = 1, message = "Min 1")
	@NotNull(message = "idPersona cannot be null")
	private String idPersona;

	@Min(value = 1, message = "Min 1")
	@NotNull(message = "tipocontacto cannot be null")
	private Integer tipocontacto;

	private String descripcin;

	@NotNull(message = "Valor cannot be null")
	private String valor;

	private Integer estadoContacto;

	/**
	 * @return the idContacto
	 */
	public Integer getIdContacto() {
		return idContacto;
	}

	/**
	 * @param idContacto the idContacto to set
	 */
	public void setIdContacto(Integer idContacto) {
		this.idContacto = idContacto;
	}

	/**
	 * @return the idPersona
	 */
	public String getIdPersona() {
		return idPersona;
	}

	/**
	 * @param idPersona the idPersona to set
	 */
	public void setIdPersona(String idPersona) {
		this.idPersona = idPersona;
	}

	/**
	 * @return the tipocontacto
	 */
	public Integer getTipocontacto() {
		return tipocontacto;
	}

	/**
	 * @return the descripcin
	 */
	public String getDescripcin() {
		return descripcin;
	}

	/**
	 * @param descripcin the descripcin to set
	 */
	public void setDescripcin(String descripcin) {
		this.descripcin = descripcin;
	}

	/**
	 * @param tipocontacto the tipocontacto to set
	 */
	public void setTipocontacto(Integer tipocontacto) {
		this.tipocontacto = tipocontacto;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @return the estadoContacto
	 */
	public Integer getEstadoContacto() {
		return estadoContacto;
	}

	/**
	 * @param estadoContacto the estadoContacto to set
	 */
	public void setEstadoContacto(Integer estadoContacto) {
		this.estadoContacto = estadoContacto;
	}

}

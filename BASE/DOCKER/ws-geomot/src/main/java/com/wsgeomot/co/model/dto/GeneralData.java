/**
 * 
 */
package com.wsgeomot.co.model.dto;

/**
 * @author RentAdvisor
 *
 */
public class GeneralData {
	private Integer type;
	private String doc;
	private String pers;
	private String placa;
	private Integer vehicleId;

	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return the doc
	 */
	public String getDoc() {
		return doc;
	}

	/**
	 * @param doc the doc to set
	 */
	public void setDoc(String doc) {
		this.doc = doc;
	}

	/**
	 * @return the pers
	 */
	public String getPers() {
		return pers;
	}

	/**
	 * @param pers the pers to set
	 */
	public void setPers(String pers) {
		this.pers = pers;
	}

	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}

	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}

	/**
	 * @return the vehicleId
	 */
	public Integer getVehicleId() {
		return vehicleId;
	}

	/**
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

}

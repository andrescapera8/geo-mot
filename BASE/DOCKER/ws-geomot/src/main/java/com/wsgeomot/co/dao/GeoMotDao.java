/**
 * 
 */
package com.wsgeomot.co.dao;

import com.wsgeomot.co.model.dto.RequesInfoGeneral;
import com.wsgeomot.co.model.dto.StatusResponse;
import com.wsgeomot.co.model.entity.ContactoEntity;
import com.wsgeomot.co.model.entity.ControlEstadoMotoEntity;
import com.wsgeomot.co.model.entity.KilometrajenotificacionEntity;
import com.wsgeomot.co.model.entity.MotoEntity;
import com.wsgeomot.co.model.entity.PersonaEntity;
import com.wsgeomot.co.model.response.ResponseContEstMotoDTOList;
import com.wsgeomot.co.model.response.ResponseContactoEntity;
import com.wsgeomot.co.model.response.ResponseEventoPlantillaDTO;
import com.wsgeomot.co.model.response.ResponseKilometrajenotificacion;
import com.wsgeomot.co.model.response.ResponseMaestraLista;
import com.wsgeomot.co.model.response.ResponseMotoEntity;
import com.wsgeomot.co.model.response.ResponseNotificaiones;
import com.wsgeomot.co.model.response.ResponsePersonaEntity;

/**
 * @author Andres Capera
 *
 */
public interface GeoMotDao {

	/**
	 * METODO DE CONSULTA DE INFORMACION GENERAL PERSONA MOTO
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return RequesInfoGeneral
	 */
	public RequesInfoGeneral getInfoMotoPersona(Integer tipoDocumento, String numDocumento, String placa, String param);

	/**
	 * METODO DE CONSULTA DE NOTIFICAIONES
	 * 
	 * @param mtIdMoto
	 * @return ResponseNotificaiones
	 */
	public ResponseNotificaiones getNotificaiones(Integer mtIdMoto);

	/**
	 * METODO DE RESGISTRO O ACTUALIZACION DE PERSONA
	 * 
	 * @param entity
	 * @return StatusResponse
	 */
	public ResponsePersonaEntity insertUpdatetPersona(PersonaEntity entity);

	/**
	 * METODO DE REGISTRO O ACTUALIZACION CONTACTO DE PERSONA
	 * 
	 * @param ContactoEntity
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdatetContacto(ContactoEntity entity);

	/**
	 * METODO DE CONSULTA DE CONTACTO DE PERSONA
	 * 
	 * @param Integer idPersona, Integer tipocontacto, String valor
	 * @return ResponseContactoEntity
	 */
	public ResponseContactoEntity getContactoEntity(String  idPersona, Integer tipocontacto, String valor);

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE MOTO
	 * 
	 * @param MotoEntity
	 * @return StatusResponse
	 */
	public ResponseMotoEntity insertUpdatetMoto(MotoEntity entity);
	
	
	/**
	 * METODO DE CONSULTA DE MOTO
	 * 
	 * @param Integer idPersona, String placa
	 * @return ResponseMotoEntity
	 */
	public ResponseMotoEntity getMotoEntity(String  idPersona, String placa);
	
	/**
	 * METODO DE CONSULTA DE MOTO POR ID
	 * 
	 * @param Integer idMoto
	 * @return ResponseMotoEntity
	 */
	public ResponseMotoEntity getMotoIdEntity(Integer idMoto);
	
	/**
	 * METODO DE CONSULTA DE MOTO POR PLACA
	 * 
	 * @param String placa
	 * @return ResponseMotoEntity
	 */
	public ResponseMotoEntity getMotoPlacaEntity(String placa);
	
	public ResponseContactoEntity getContactoIdEntity(Integer idContacto);

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE ESTADO KILOMETRAJE MOTO
	 * 
	 * @param ControlEstadoMotoEntity
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdatetControlEstadoMoto(ControlEstadoMotoEntity entity);

	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO
	 * 
	 * @param KilometrajenotificacionEntity
	 * @return StatusResponse
	 */
	public ResponseKilometrajenotificacion insertUpdatetKilometrajenotificacion(KilometrajenotificacionEntity entity);
	
	
	/**
	 * METODO DE CONSULTA DE KILOMETRAJE DE NOTIFICACION DE MOTO
	 * 
	 * @param KilometrajenotificacionEntity
	 * @return ResponseKilometrajenotificacion
	 */
	public ResponseKilometrajenotificacion getKilometrajenotificacion(Integer mtidmoto, Integer evtidevento, Double kilometrajevalidador, Double kntkilometraje);
	
	
	/**
	 * METODO DE CONSULTA DE KILOMETRAJE POR ID DE NOTIFICACION DE MOTO
	 * 
	 * @param Integer knidkilometrajenoti
	 * @return ResponseKilometrajenotificacion
	 */
	public ResponseKilometrajenotificacion getKilometraIdjenotificacion(Integer knidkilometrajenoti);

	/**
	 * METODO DE CONSULTA DE LISTAS MAESTRAS
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return ResponseMaestraLista
	 */
	public ResponseMaestraLista getInfoListasMaestras(String tipoDato);

	/**
	 * METODO DE CONSULTA DE EVENTOS Y PLANTILLAS
	 * 
	 * @param idEvento
	 * @returnResponseEventoPlantillaDTO
	 */
	public ResponseEventoPlantillaDTO getEventoPlantilla(Integer idEvento);
	
	/**
	 * METODO DE CONSULTA DE RECORRIDOS
	 * 
	 * @param idMoto, idEvento
	 * @return ResponseContEstMotoDTOList
	 */
	public ResponseContEstMotoDTOList getControlEstadoMoto(Integer idMoto, Integer idEvento);

}

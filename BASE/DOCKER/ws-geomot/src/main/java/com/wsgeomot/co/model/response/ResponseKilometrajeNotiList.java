/**
 * 
 */
package com.wsgeomot.co.model.response;

import java.util.List;

import com.wsgeomot.co.model.dto.StatusResponse;

/**
 * @author Andres Capera
 *
 */
public class ResponseKilometrajeNotiList {
	private List<ResponseKilometrajenotificacion> responseKilometrajeNotiList;
	private StatusResponse statusResponse;

	/**
	 * @return the responseKilometrajeNotiList
	 */
	public List<ResponseKilometrajenotificacion> getResponseKilometrajeNotiList() {
		return responseKilometrajeNotiList;
	}

	/**
	 * @param responseKilometrajeNotiList the responseKilometrajeNotiList to set
	 */
	public void setResponseKilometrajeNotiList(List<ResponseKilometrajenotificacion> responseKilometrajeNotiList) {
		this.responseKilometrajeNotiList = responseKilometrajeNotiList;
	}

	/**
	 * @return the statusResponse
	 */
	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	/**
	 * @param statusResponse the statusResponse to set
	 */
	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}

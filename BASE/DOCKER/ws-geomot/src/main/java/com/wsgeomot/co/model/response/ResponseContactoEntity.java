/**
 * 
 */
package com.wsgeomot.co.model.response;

import com.wsgeomot.co.model.dto.StatusResponse;
import com.wsgeomot.co.model.entity.ContactoEntity;

/**
 * @author RentAdvisor
 *
 */
public class ResponseContactoEntity {

	private ContactoEntity contactoEntity;
	private StatusResponse statusResponse;

	/**
	 * @return the contactoEntity
	 */
	public ContactoEntity getContactoEntity() {
		return contactoEntity;
	}

	/**
	 * @param contactoEntity the contactoEntity to set
	 */
	public void setContactoEntity(ContactoEntity contactoEntity) {
		this.contactoEntity = contactoEntity;
	}

	/**
	 * @return the statusResponse
	 */
	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	/**
	 * @param statusResponse the statusResponse to set
	 */
	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}

/**
 * 
 */
package com.wsgeomot.co.service;

import com.wsgeomot.co.model.dto.ContactoDTO;
import com.wsgeomot.co.model.dto.ControlEstadoMotoDTO;
import com.wsgeomot.co.model.dto.ControlEstadoMotoDTOList;
import com.wsgeomot.co.model.dto.KilometrajenotiListDTO;
import com.wsgeomot.co.model.dto.KilometrajenotificacionDTO;
import com.wsgeomot.co.model.dto.MotoDTO;
import com.wsgeomot.co.model.dto.PersonaContactoRequest;
import com.wsgeomot.co.model.dto.PersonaDTO;
import com.wsgeomot.co.model.dto.RequesInfoGeneral;
import com.wsgeomot.co.model.dto.StatusResponse;
import com.wsgeomot.co.model.response.ControlEstadoMotoDTOResponseGN;
import com.wsgeomot.co.model.response.ResponseContEstMotoDTOList;
import com.wsgeomot.co.model.response.ResponseContactoGL;
import com.wsgeomot.co.model.response.ResponseEventoPlantillaDTO;
import com.wsgeomot.co.model.response.ResponseKilometrajeNotiList;
import com.wsgeomot.co.model.response.ResponseKilometrajenotificacion;
import com.wsgeomot.co.model.response.ResponseMaestraLista;
import com.wsgeomot.co.model.response.ResponseMotoEntity;
import com.wsgeomot.co.model.response.ResponseNotificaiones;
import com.wsgeomot.co.model.response.ResponsePersonaEntity;

/**
 * @author Andres Capera
 *
 */
public interface GeoMotService {

	/**
	 * METODO DE CONSULTA DE INFORMACION GENERAL PERSONA MOTO
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return RequesInfoGeneral
	 */
	public RequesInfoGeneral getInfoMotoPersona(Integer tipoDocumento, String numDocumento, String placa, String param);

	/**
	 * METODO DE REGISTRO DE PERSONA Y CONTACTO
	 * 
	 * @param persoContacRequest
	 * @return ResponseContactoGL
	 */
	public ResponseContactoGL insertUpdatePersonaContactoDTO(PersonaContactoRequest persoContacRequest);

	/**
	 * METODO DE RESGISTRO O ACTUALIZACION DE PERSONA
	 * 
	 * @param persona
	 * @return insertUpdatePersonaDTO
	 */
	public ResponsePersonaEntity insertUpdatePersonaDTO(PersonaDTO persona);

	/**
	 * METODO DE REGISTRO O ACTUALIZACION CONTACTO DE PERSONA
	 * 
	 * @param contacto
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdateContactoDTO(ContactoDTO contacto);

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE MOTO
	 * 
	 * @param motoDTO
	 * @return StatusResponse
	 */
	public ResponseMotoEntity insertUpdateMotoDTO(MotoDTO motoDTO);

	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE ESTADO KILOMETRAJE MOTO
	 * 
	 * @param controlEstadoMoto
	 * @return StatusResponse
	 */
	public StatusResponse insertUpdateContEstadoMotoDTO(ControlEstadoMotoDTO controlEstadoMoto);
	
	/**
	 * METODO DE REGISTRO O ACTUALIZACION DE LISTA ESTADO KILOMETRAJE MOTO
	 * 
	 * @param ControlEstadoMotoDTO
	 * @return StatusResponse
	 */
	public ControlEstadoMotoDTOResponseGN insertUpdateContEstadoMotoDTOList(ControlEstadoMotoDTOList controlEstadoMotoList);

	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO
	 * 
	 * @param kilometrajenotificacion
	 * @return StatusResponse
	 */
	public ResponseKilometrajenotificacion insertUpdateKilometrajenotificacionDTO(
			KilometrajenotificacionDTO kilometrajenotificacion);

	/**
	 * METODO DE REGISTRO DE KILOMETRAJE DE NOTIFICACION DE MOTO LISTA
	 * 
	 * @param KilometrajenotiListDTO
	 * @return ResponseKilometrajeNotiList
	 */
	public ResponseKilometrajeNotiList insertUpdateKilometrajenotificacionListDTO(KilometrajenotiListDTO kilometrajenotificacion);

	/**
	 * METODO DE CONSULTA DE LISTAS MAESTRAS
	 * 
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param placa
	 * @return ResponseMaestraLista
	 */
	public ResponseMaestraLista getInfoListasMaestras(String tipoDato);

	/**
	 * METODO DE CONSULTA DE EVENTOS Y PLANTILLAS
	 * 
	 * @param idEvento
	 * @returnResponseEventoPlantillaDTO
	 */
	public ResponseEventoPlantillaDTO getEventoPlantilla(Integer idEvento);
	
	
	/**
	 * METODO DE CONSULTA DE NOTIFICAIONES
	 * 
	 * @param mtIdMoto
	 * @return ResponseNotificaiones
	 */
	public ResponseNotificaiones getNotificaiones(Integer mtIdMoto);
	
	/**
	 * METODO DE CONSULTA DE RECORRIDOS
	 * 
	 * @param idMoto, idEvento
	 * @return ResponseContEstMotoDTOList
	 */
	public ResponseContEstMotoDTOList getControlEstadoMoto(Integer idMoto, Integer idEvento);

}

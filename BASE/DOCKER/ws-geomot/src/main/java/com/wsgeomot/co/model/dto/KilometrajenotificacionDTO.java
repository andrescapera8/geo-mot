package com.wsgeomot.co.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class KilometrajenotificacionDTO {

	private Integer knidkilometrajenoti;

	@Min(value = 1, message = "Min 1")
	@NotNull(message = "mtidmoto cannot be null")
	private Integer mtidmoto;

	@Min(value = 1, message = "Min 1")
	@NotNull(message = "evtidevento cannot be null")
	private Integer evtidevento;

	private String descripcin;

	@NotNull(message = "kilometrajevalidador cannot be null")
	private Double kilometrajevalidador;

	@NotNull(message = "kntkilometraje cannot be null")
	private Double kntkilometraje;

	private String ntfecha;

	private Integer stidestado;

	/**
	 * @return the knidkilometrajenoti
	 */
	public Integer getKnidkilometrajenoti() {
		return knidkilometrajenoti;
	}

	/**
	 * @param knidkilometrajenoti the knidkilometrajenoti to set
	 */
	public void setKnidkilometrajenoti(Integer knidkilometrajenoti) {
		this.knidkilometrajenoti = knidkilometrajenoti;
	}

	/**
	 * @return the mtidmoto
	 */
	public Integer getMtidmoto() {
		return mtidmoto;
	}

	/**
	 * @param mtidmoto the mtidmoto to set
	 */
	public void setMtidmoto(Integer mtidmoto) {
		this.mtidmoto = mtidmoto;
	}

	/**
	 * @return the evtidevento
	 */
	public Integer getEvtidevento() {
		return evtidevento;
	}

	/**
	 * @param evtidevento the evtidevento to set
	 */
	public void setEvtidevento(Integer evtidevento) {
		this.evtidevento = evtidevento;
	}

	/**
	 * @return the descripcin
	 */
	public String getDescripcin() {
		return descripcin;
	}

	/**
	 * @param descripcin the descripcin to set
	 */
	public void setDescripcin(String descripcin) {
		this.descripcin = descripcin;
	}

	/**
	 * @return the kilometrajevalidador
	 */
	public Double getKilometrajevalidador() {
		return kilometrajevalidador;
	}

	/**
	 * @param kilometrajevalidador the kilometrajevalidador to set
	 */
	public void setKilometrajevalidador(Double kilometrajevalidador) {
		this.kilometrajevalidador = kilometrajevalidador;
	}

	/**
	 * @return the kntkilometraje
	 */
	public Double getKntkilometraje() {
		return kntkilometraje;
	}

	/**
	 * @param kntkilometraje the kntkilometraje to set
	 */
	public void setKntkilometraje(Double kntkilometraje) {
		this.kntkilometraje = kntkilometraje;
	}

	/**
	 * @return the ntfecha
	 */
	public String getNtfecha() {
		return ntfecha;
	}

	/**
	 * @param ntfecha the ntfecha to set
	 */
	public void setNtfecha(String ntfecha) {
		this.ntfecha = ntfecha;
	}

	/**
	 * @return the stidestado
	 */
	public Integer getStidestado() {
		return stidestado;
	}

	/**
	 * @param stidestado the stidestado to set
	 */
	public void setStidestado(Integer stidestado) {
		this.stidestado = stidestado;
	}

}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './components/pages/auth/auth.component';
import { ContactosComponent } from './components/pages/contactos/contactos.component';
import { PersonalDataComponent } from './components/pages/personal-data/personal-data.component';
import { ServiceConfigComponent } from './components/pages/service-config/service-config.component';
import { VehicleDataComponent } from './components/pages/vehicle-data/vehicle-data.component';
import { AuthGuard } from './guards/authGuard.guard';
import { LoginGuard } from './guards/loginGuard.guard';


const routes: Routes = [
  {
    path: 'login',
    component: AuthComponent,
    canActivate: []
  },
  {
    path: 'contactos',
    component: ContactosComponent,
    canActivate: []
  },
  {
    path: 'datos-personales',
    component: PersonalDataComponent,
    canActivate: []
  },
  {
    path: 'vehiculo',
    component: VehicleDataComponent,
    canActivate: []
  },
  {
    path: 'configuracion-servicio',
    component: ServiceConfigComponent,
    canActivate: []
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

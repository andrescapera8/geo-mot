import { Injectable } from '@angular/core';

import { ApiManagerService } from '../../common/api.manager.service';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RegisterVehicleService {

  constructor(
    private _api: ApiManagerService,
  ) { }

  /**
   *  Guarda la información del usuario a registrar
   */
  saveVehicleInformation(payload) {
    const endpoint = environment.saveVehicleData;

    return this._api.post(endpoint, payload)
  }
}

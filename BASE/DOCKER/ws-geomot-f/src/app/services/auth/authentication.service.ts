import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { BehaviorSubject } from 'rxjs';

const TOKEN_KEY = 'my-token';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor() { }

  /**
   *  Funcion que realiza login con Google
   */
  googleLogin() {
    return GoogleAuth.signIn()
      .then((data) => {
        if (data) {
          this.isAuthenticated.next(true);
          return data.authentication.accessToken;
        } else {
          this.isAuthenticated.next(false);
        }
      });
  }

  /**
   *  Funcion que refresca token
   */
  refreshTokenGoogle() {
    return GoogleAuth.refresh()
      .then((data) => {
        if (data) {
          return data;
        }
      });
  }

  /**
   *  Funcion que realiza logout con Google
   */
  googleLogout(): Promise<void> {
    this.isAuthenticated.next(false);
    return Storage.remove({ key: TOKEN_KEY });
  }

  getAuthentication() {
    return this.isAuthenticated.value;
  }
}

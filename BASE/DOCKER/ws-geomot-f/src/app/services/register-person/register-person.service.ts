import { Injectable } from '@angular/core';

import { ApiManagerService } from './../../common/api.manager.service';
import { environment } from './../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RegisterPersonService {

  constructor(
    private _api: ApiManagerService,
  ) { }

  /**
   *  Guarda la información del usuario a registrar
   */
  savePersonalInformation(payload) {
    const endpoint = environment.savePersonalData;

    return this._api.post(endpoint, payload)
  }

  /**
   *  Guarda la información del usuario a registrar
   */
  getInfoPerson(typDoc, doc, param, licPlate = '') {
    const endpoint = environment.getPersonID +
      `?tipoDocumento=${typDoc}&numDocumento=${doc}&param=${param}&placa=${licPlate}`;

    return this._api.get(endpoint)
  }
}

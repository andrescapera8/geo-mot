import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiManagerService } from './common/api.manager.service';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { AuthComponent } from './components/pages/auth/auth.component';
import { ContactosComponent } from './components/pages/contactos/contactos.component';
import { HomeComponent } from './components/pages/home/home.component';
import { PersonalDataComponent } from './components/pages/personal-data/personal-data.component';
import { ServiceConfigComponent } from './components/pages/service-config/service-config.component';
import { VehicleDataComponent } from './components/pages/vehicle-data/vehicle-data.component';
import { SharedModule } from './components/shared/shared.module';
import { InterceptedService } from './interceptor/intercepted.service';
import { HttpHandlerService } from './utils/HttpHandler.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ContactosComponent,
    PersonalDataComponent,
    VehicleDataComponent,
    ServiceConfigComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
    }),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    LoadingBarHttpClientModule
  ],
  entryComponents: [],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptedService, multi: true },
    ApiManagerService,
    HttpHandlerService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

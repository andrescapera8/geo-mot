import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@capacitor/storage';
import { AuthenticationService } from '../../../services/auth/authentication.service';

const TOKEN_KEY = 'my-token';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

  @Input() $title: any;
  @Input() $icon: any;

  constructor(
    private router: Router,
    private _authSrv: AuthenticationService,
  ) { }

  ngOnInit() { }


  async signOut() {
    await this._authSrv.googleLogout();
    this.router.navigateByUrl('/login', { replaceUrl: true });
  }

}

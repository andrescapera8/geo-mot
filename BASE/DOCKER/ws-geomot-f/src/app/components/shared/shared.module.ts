import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ContactDataComponent } from './personal-data/contact-data/contact-data.component';
import { RegisterUserComponent } from './personal-data/register-user/register-user.component';
import { EventsTableComponent } from './service-config/events-table/events-table.component';
import { FormEventsComponent } from './service-config/form-events/form-events.component';
import { ToolbarComponent } from './toolbar/toolbar.component';


@NgModule({
  declarations: [
    ToolbarComponent,
    RegisterUserComponent,
    ContactDataComponent,
    EventsTableComponent,
    FormEventsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    IonicModule
  ],
  exports: [
    ToolbarComponent,
    RegisterUserComponent,
    ContactDataComponent,
    NgxDatatableModule,
    EventsTableComponent,
    FormEventsComponent
  ]
})
export class SharedModule { }

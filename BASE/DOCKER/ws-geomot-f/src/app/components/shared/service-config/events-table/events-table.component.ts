import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ServiceConfigDTO } from '../../../../models/service-config/ServiceConfigDTO';


@Component({
  selector: 'app-events-table',
  templateUrl: './events-table.component.html',
  styleUrls: ['./events-table.component.scss'],
})
export class EventsTableComponent implements OnInit {

  @Output() dataEvents = new EventEmitter();

  @Input() $eventsTable: any;
  @Input() $submit: any;


  events: ServiceConfigDTO[] = [];


  constructor() { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.$eventsTable && this.$eventsTable) {
      const table = [...this.events];
      this.events = [];

      this.events = table.concat(this.$eventsTable);
    }

    if (changes.$submit && this.$submit > 0 && this.events.length > 0) {
      this.dataEvents.emit(this.events);
    }
  }

  deleteEvn(index) {
    this.events.splice(index, 1);
    const table = [...this.events];
    this.events = [];

    this.events = table;
  }
}

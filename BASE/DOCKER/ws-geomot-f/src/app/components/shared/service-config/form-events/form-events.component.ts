import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ListasMaestrasService } from 'src/app/services/listas-maestras/listas-maestras.service';
import { ServiceConfigDTO } from '../../../../models/service-config/ServiceConfigDTO';
import { EventsTableComponent } from '../events-table/events-table.component';


@Component({
  selector: 'app-form-events',
  templateUrl: './form-events.component.html',
  styleUrls: ['./form-events.component.scss'],
})
export class FormEventsComponent implements OnInit {

  @Output() eventNew = new EventEmitter();


  event: ServiceConfigDTO = new ServiceConfigDTO();

  listaMaestraEventosDTO: any;
  listaMaestraNumEventosDTO: any;

  pld: any = {};

  private stopSubcription$ = new Subject<void>();


  constructor(
    private _listaMaestras: ListasMaestrasService,
  ) { }

  ngOnInit() {
    this.pld = JSON.parse(localStorage.getItem('handler'));

    combineLatest(
      this._listaMaestras.getEvents(1),
      this._listaMaestras.getListasMaestras('CILINDRAJE'),
    )
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(([Data1, Data2]) => {
        if (Data1.statusResponse.status == 200) {
          this.listaMaestraEventosDTO = Data1.eventoLista;
        }

        if (Data2.statusResponse.status == 200) {
          // this.listaMaestraNumEventosDTO = Data2.listaMaestraDTO;
        }
      });
  }

  addEvent() {
    this.event.mtidmoto = this.pld?.vehicle;

    this.eventNew.emit(this.event);
    this.event = new ServiceConfigDTO();
  }


  /**
   *  Cambia valores de los select
   */
  onChangeEvent(data) {
    this.event.evtidevento = data.value.id;
  }

}

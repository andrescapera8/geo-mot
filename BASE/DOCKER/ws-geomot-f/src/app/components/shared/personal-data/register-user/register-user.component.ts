import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PersonalDataDTO } from '../../../../models/register-data/PersonalDataDTO';
import { ListasMaestrasService } from '../../../../services/listas-maestras/listas-maestras.service';


@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss'],
})
export class RegisterUserComponent implements OnInit {

  @ViewChild('registerForm') registerForm: NgForm;

  @Output() userDataRegister = new EventEmitter();

  @Input() $submit: any;
  @Input() $getPersData: any;

  /**
   * LISTAS MAESTRAS MODELOS
   */
  listaMaestraGeneroDTO: any;
  listaMaestraTipoDocumDTO: any;
  listaMaestraGrupoSangDTO: any;

  regisPers: PersonalDataDTO = new PersonalDataDTO();
  photoName: string = '';

  private stopSubcription$ = new Subject<void>();


  constructor(
    private _listaMaestras: ListasMaestrasService,
  ) { }

  ngOnInit() {
    combineLatest(
      this._listaMaestras.getListasMaestras('TIPOSEXO'),
      this._listaMaestras.getListasMaestras('TIPODOCUMENTO'),
      this._listaMaestras.getListasMaestras('TIPORH')
    )
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(([Data1, Data2, Data3]) => {
        if (Data1.statusResponse.status == 200) {
          this.listaMaestraGeneroDTO = Data1.listaMaestraDTO;
        }

        if (Data2.statusResponse.status == 200) {
          this.listaMaestraTipoDocumDTO = Data2.listaMaestraDTO;
        }

        if (Data3.statusResponse.status == 200) {
          this.listaMaestraGrupoSangDTO = Data3.listaMaestraDTO;
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.$submit > 0 && this.registerForm.valid) {
      this.register();
    }

    if (this.$getPersData) {
      this.$getPersData.fechaNacimiento = this.$getPersData.fechaNacimiento.substring(0, 10);
      this.regisPers = this.$getPersData;
    }
  }

  /**
   * Obtine la fecha seleccionada
   */
  formatDate(value) {
    this.regisPers.fechaNacimiento = value;
  }

  /**
   *  Adjunta archivo de fotografia en base64
   */
  onFileSelected(event) {
    const file = event.target.files[0];
    this.photoName = file.name;
    const reader = new FileReader();

    reader.onload = (e: any) => {
      this.regisPers.foto = e.target.result;
    };

    reader.readAsDataURL(file);
  }


  /**
   *  Cambia valores de los select
   */
  onChangeTypeDoc(data) {
    this.regisPers.tipoDocumento = data.value;
  }

  onChangeGender(data) {
    this.regisPers.sexo = data.value;
  }

  onChangeRH(data) {
    this.regisPers.tipoRh = data.value;
  }

  register() {
    this.userDataRegister.emit(this.regisPers);
  }


}

import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { combineLatest } from "rxjs";
import { takeUntil } from 'rxjs/operators';

import { ContactDataDTO } from '../../../../models/register-data/ContactDataDTO';
import { ListasMaestrasService } from '../../../../services/listas-maestras/listas-maestras.service';


@Component({
  selector: 'app-contact-data',
  templateUrl: './contact-data.component.html',
  styleUrls: ['./contact-data.component.scss'],
})
export class ContactDataComponent implements OnInit {

  @Output() contactDataRegister = new EventEmitter();

  @Input() $submit: any;
  @Input() $getContData: any;

  contacts: ContactDataDTO[] = [];
  contactInit: ContactDataDTO = new ContactDataDTO();

  listaMaestraContactoDTO: any;

  tablestyle = 'bootstrap'

  index: number = null;

  private stopSubcription$ = new Subject<void>();


  constructor(
    private modalCtrl: ModalController,
    private _listaMaestras: ListasMaestrasService,
  ) { }

  ngOnInit() {
    this._listaMaestras.getListasMaestras('CONTACTO')
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          this.listaMaestraContactoDTO = resp.listaMaestraDTO;
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.$getContData) {
      this.contacts = this.$getContData;
    }
  }


  addContacts() {
    const table = this.contacts;
    this.contacts = [];

    if (this.index) {
      table[this.index] = { ...this.contactInit };
      this.index = null;
      this.contactInit = new ContactDataDTO();
    } else {
      table.push({ ...this.contactInit })
    }

    this.contacts = [...table];
  }

  /**
   *  Cambia valores de los select
   */
  onChangeTypeCont(data) {
    this.contactInit.tipocontacto = data.value.id;
  }

  sendContData() {
    this.contactDataRegister.emit(this.contacts);

    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  deleteContact(index) {
    this.contacts.splice(index, 1);
    const table = [...this.contacts];
    this.contacts = [];

    this.contacts = table;
  }

  editContact(data, index) {
    this.index = index;
    this.contactInit = { ...data };
  }
}

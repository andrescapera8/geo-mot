import { Component, OnInit } from '@angular/core';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { vehiculeDTO } from '../../../models/register-vehicule/vehiculeDTO';
import { ListasMaestrasService } from '../../../services/listas-maestras/listas-maestras.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterPersonService } from '../../../services/register-person/register-person.service';
import { RegisterVehicleService } from '../../../services/register-vehicle/register-vehicle.service';


@Component({
  selector: 'app-vehicle-data',
  templateUrl: './vehicle-data.component.html',
  styleUrls: ['./vehicle-data.component.scss'],
})
export class VehicleDataComponent implements OnInit {

  regVihicule: vehiculeDTO = new vehiculeDTO();

  private stopSubcription$ = new Subject<void>();

  listaMaestraMarcaDTO: any;
  listaMaestraColorDTO: any;
  listaMaestraCombustibleDTO: any;
  listaMaestraCilindrajeDTO: any;

  idPersona: number;

  pld: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _listaMaestras: ListasMaestrasService,
    private _regVehicleSrv: RegisterVehicleService,
    private _registerPerSrv: RegisterPersonService
  ) { }

  ngOnInit() {
    this.pld = JSON.parse(localStorage.getItem('handler'));

    combineLatest(
      this._listaMaestras.getListasMaestras('MARCA'),
      this._listaMaestras.getListasMaestras('COLOR'),
      this._listaMaestras.getListasMaestras('COMBUSTILBE'),
      this._listaMaestras.getListasMaestras('CILINDRAJE'),
      this._registerPerSrv.getInfoPerson(this.pld?.type, this.pld?.doc, 'MOTO', this.pld?.placa),
    )
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(([Data1, Data2, Data3, Data4, Data5]) => {
        if (Data1.statusResponse.status == 200) {
          this.listaMaestraMarcaDTO = Data1.listaMaestraDTO;
        }

        if (Data2.statusResponse.status == 200) {
          this.listaMaestraColorDTO = Data2.listaMaestraDTO;
        }

        if (Data3.statusResponse.status == 200) {
          this.listaMaestraCombustibleDTO = Data3.listaMaestraDTO;
        }

        if (Data4.statusResponse.status == 200) {
          this.listaMaestraCilindrajeDTO = Data4.listaMaestraDTO;
        }

        if (Data5.statusResponse.status == 200) {
          this.regVihicule = Data5.motoDTO;
        }
      });
  }


  registerVihicle() {
    this.regVihicule.idPersona = this.pld.pers;

    this._regVehicleSrv.saveVehicleInformation(this.regVihicule)
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          this.pld.placa = resp.motoEntity.placa;
          this.pld.vehicle = resp.motoEntity.idMoto;

          localStorage.setItem('handler', JSON.stringify(this.pld));
          this.router.navigate(['/configuracion-servicio'])
        }
      });
  }

  onChangeMarca(data) {
    this.regVihicule.marca = data.value;
  }

  onChangeCombustible(data) {
    this.regVihicule.combustilbe = data.value;
  }

  onChangeColor(data) {
    this.regVihicule.combustilbe = data.value;
  }

  onChangeCilindraje(data) {
    this.regVihicule.cleidcilindraje = data.value;
  }

}

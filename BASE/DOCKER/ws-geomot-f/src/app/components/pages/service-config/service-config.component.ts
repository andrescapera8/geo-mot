import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MileageDTO } from '../../../models/service-config/MileageDTO';
import { ServiceConfigDTO } from '../../../models/service-config/ServiceConfigDTO';
import { RegisterPersonService } from '../../../services/register-person/register-person.service';
import { ServiceConfigService } from '../../../services/service-config/service-config.service';


@Component({
  selector: 'app-service-config',
  templateUrl: './service-config.component.html',
  styleUrls: ['./service-config.component.scss'],
})
export class ServiceConfigComponent implements OnInit {

  sendServiceConfig: ServiceConfigDTO[] = [];
  miliage: MileageDTO = new MileageDTO();

  event$: any;

  submit: number = 0;

  pld: any = {};

  private stopSubcription$ = new Subject<void>();


  constructor(
    private _servCongSrv: ServiceConfigService,
    private toastCtrl: ToastController,
    private registerPerSrv: RegisterPersonService,
  ) { }

  ngOnInit() {
    this.pld = JSON.parse(localStorage.getItem('handler'));

    this.registerPerSrv.getInfoPerson(this.pld?.type, this.pld?.doc, 'KILOMETRAJENOTI', this.pld?.placa)
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(res => {
        if (res.statusResponse.status == 200) {
          this.event$ = res.kilometrajenotificacionDTOList;
        }
      });
  }

  sendSubmit() {
    this.submit += 1;
  }

  catchEventsData($event) {
    this.sendServiceConfig = $event;

    this.save();
  }

  save() {
    this.miliage.idMoto = this.pld?.vehicle;

    combineLatest(
      this._servCongSrv.saveEvents({ kilometrajenotificacionListDTO: this.sendServiceConfig }),
      this._servCongSrv.registerMileage(this.miliage),
    )
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe(([Data1, Data2]) => {
        if (Data1.statusResponse.status == 200 && Data2.statusResponse.status == 200) {
          this.openToast()
        }
      });
  }

  async openToast() {
    const toast = await this.toastCtrl.create({
      message: 'Información guardada correctamente.',
      duration: 2500,
    });
    toast.present();
  }
}

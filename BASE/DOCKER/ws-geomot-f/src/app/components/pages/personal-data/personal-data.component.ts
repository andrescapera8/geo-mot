import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonRouterOutlet, ToastController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RegisterPersonalDataDTO } from '../../../models/register-data/RegisterPersonalDataDTO';
import { RegisterPersonService } from '../../../services/register-person/register-person.service';



@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss'],
})
export class PersonalDataComponent implements OnInit {

  sendRegister: RegisterPersonalDataDTO = new RegisterPersonalDataDTO();

  submit: number = 0;

  pld: any = {};

  private stopSubcription$ = new Subject<void>();


  constructor(
    private router: Router,
    public routerOutlet: IonRouterOutlet,
    private registerPerSrv: RegisterPersonService,
    private toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    this.pld = JSON.parse(localStorage.getItem('handler'));

    this.registerPerSrv.getInfoPerson(this.pld?.type, this.pld?.doc, 'PERSONA_CONTACTO')
      // this.registerPerSrv.getInfoPerson('1', '1000', 'PERSONA_CONTACTO')
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          this.sendRegister = resp;
        }
      });
  }


  sendSubmit() {
    this.submit += 1;
  }

  catchPersonRegister($event) {
    if (this.sendRegister.contactoDTOList.length > 0) {
      this.sendRegister.personaDTO = $event;

      this.saveRegister();
    } else {
      this.openToast();
    }
  }

  catchContactsRegister($event) {
    this.sendRegister.contactoDTOList = $event;
  }


  saveRegister() {
    this.registerPerSrv.savePersonalInformation(this.sendRegister)
      .pipe(takeUntil(this.stopSubcription$))
      .subscribe((resp: any) => {
        if (resp.statusResponse.status == 200) {
          this.pld = {};
          this.pld.type = resp.persona.tipoDocumento;
          this.pld.doc = resp.persona.numDocumento;
          this.pld.pers = resp.persona.idPersona;

          localStorage.setItem('handler', JSON.stringify(this.pld));
          this.router.navigate(['/vehiculo'])
        }
      });
  }


  async openToast() {
    const toast = await this.toastCtrl.create({
      message: 'Debe agregar una persona de contacto para registro.',
      duration: 2500,
    });
    toast.present();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@capacitor/storage';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { isPlatform } from '@ionic/angular';
import { AuthenticationDTO } from '../../../models/auth/AuthenticationDTO';
import { AuthenticationService } from '../../../services/auth/authentication.service';


const TOKEN_KEY = 'my-token';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {

  auth: AuthenticationDTO = new AuthenticationDTO();


  constructor(
    private router: Router,
    private _authSrv: AuthenticationService
  ) {
    if (!isPlatform('capacitor')) {
      GoogleAuth.initialize();
    }
  }

  ngOnInit() { }

  signIn() {
    this._authSrv.googleLogin()
      .then(async (token) => {
        if (token) {
          Storage.set({ key: TOKEN_KEY, value: token });
          this.router.navigate(['/datos-personales'])
        }
      });
  }

  refresh() {
    this._authSrv.googleLogin()
      .then((token) => {
        if (token) {
          Storage.set({ key: TOKEN_KEY, value: token });
          console.log('refresh:', token)
        }
      });
  }
}

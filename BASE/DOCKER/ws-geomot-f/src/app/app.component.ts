import { Component } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { LoadingController, LoadingOptions } from '@ionic/angular';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  ctrlPercent: boolean = true;


  constructor(
    public loadingBarService: LoadingBarService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.loadingBarService.value$.subscribe(async (resp) => {
      let totalLoader = parseInt(resp.toString());

      if (totalLoader > 0 && this.ctrlPercent) {
        this.presentLoadingWithOptions();
        this.ctrlPercent = false;
      } else if (totalLoader == 0) {
        await this.closeLoader();
        this.ctrlPercent = true;
      }
    });
  }


  presentLoadingWithOptions() {
    this.loadingCtrl.create({
      spinner: 'bubbles',
      translucent: true,
      cssClass: 'custom-class custom-loading',
    }).then((res) => res.present());
  }

  closeLoader() {
    this.loadingCtrl.dismiss().then((res) => res).catch((error) => error);
  }

  // const { role, data } = ;
  // console.log('Loading dismissed with role:', role);
}

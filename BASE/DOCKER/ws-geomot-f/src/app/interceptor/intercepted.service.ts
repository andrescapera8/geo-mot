import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
// import { SpinnerService } from "./spinner.service";
import { finalize } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InterceptedService implements HttpInterceptor {

  constructor(
    // private spinnerService: SpinnerService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      // finalize(() => this.spinnerService.stopSpinner())
    );
  }
}
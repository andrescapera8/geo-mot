import { ContactDataDTO } from "./ContactDataDTO"
import { PersonalDataDTO } from "./PersonalDataDTO"


export class RegisterPersonalDataDTO {
	personaDTO: PersonalDataDTO;
	contactoDTOList: ContactDataDTO[] = [];
}

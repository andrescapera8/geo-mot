


export class PersonalDataDTO {
	idPersona: any;
	tipoDocumento: any;
	numDocumento: any;
	nombres: any;
	apellidos: any;
	sexo: any;
	firmaDigital: any;
	foto: any;
	fechaNacimiento: any;
	estatura: any;
	peso: any;
	tipoRh: any;
}
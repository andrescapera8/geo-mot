


export class ServiceConfigDTO {
  knidkilometrajenoti: any = null; // ID kilometraje de notificacion
  evtidevento: any;
  mtidmoto: any;
  kntkilometraje: any; // Kilometraje de notificacion
  kilometrajevalidador: any; // Kilometraje de la revision del evento
  ntduracion: any;
  ntnumrepeticion: any;
  nttiemporepeticion: any;
  stidestado: any = null;


  evtideventoString: any;
}